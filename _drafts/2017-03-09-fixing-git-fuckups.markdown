---
layout: post
title:  Fixing git fuckup
ref:    fixing-git-fuckup
date:   2017-03-09 18:25:00 +01:00
lang:   en
orig-lang: en
author: matthias
---

When I was about to merge a huge merge request with lots of spelling mistakes and changes, it happened again:
Shortly after the merge I noticed that it does not pass my personal quality criteria.

One problem is, that these criteria should be written down somewhere and not only reside in my head and some peoples' assumptions but the problem I want to focus on here is: How do I undo a big merge request, fix the problems and reapply it later?

<!--more-->

Unfortunately, the solution I chose and I will present here is not the favorite one you would expect.

The situation here is that 64 commits touch a lot of files that should not have been touched as well as introduce some changes I want to keep for later because they affect some production dumps which need to be fed back to the production system to lead to the fixes becoming effective.

### Reverting the merge request

Although documentation tells so, I did not find a revert button in the merge request in gitlab. So use `git log` to find out the last good revision before the merge and `git revert --no-edit last_good_revision..` to revert all changes afterwards.

This generates the same number of commits again, maybe you want to squash them into one, when the verbosity is not needed. I decided against and just pushed to master again.

### Getting the changes to reapply

Now I want to get the changes again, do some edits and finally merge.

Turns out, that is not as easy as expected: Git knows the changes are already in the tree, so I did not find a solution to rebase them ontop a second time. If you search for solutions, some suggest `rebase -f` or rewording the oldest commit to trick git into thinking there is a difference. You can save your time, it does not work.

`git cherry-pick` is the tool of choise: It takes a commit and just does it again.

If you now just throw `cherry-pick` at the original range of revisions, it will fail, because the order in which the changes are applied is wrong. By using `git rev-list`, we can work around that:

`git rev-list --reverse oldest_commit_to_copy^..newest_commit_to_copy | git cherry-pick --stdin`

Because this is just all spelling mistakes and no real commit structure is needed, I will make one commit out of all of them so it is also way easier for me to remove all files from the changeset I do not want to be committed. So I added another `-n` parameter to the cherry-pick for it not to create commits.

In the end, this merge request took me four hours. But this blog post is also a result from it :-)
