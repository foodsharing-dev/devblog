---
layout: post
title:  Issue guidelines
ref:    issue-guidelines
date:   2017-06-30 14:38:00 +02:00
lang:   en
orig-lang: en
author: matthias
---

There are multiple projects, multiple repositories as well as multiple issue trackers currently being used for foodsharing.
This document should give some hints about what should go where.

Please keep in mind that sensitive data (e.g. personal data) should _never_ be shared through any kind of issue tracker. Always directly get in contact with Matthias ([foodsharing-contact](foodsharing-contact@matthias-larisch.de)) for these kinds of problems (but keep in mind that he will provide personal support and administration with very little priority)!

There is [GitLab foodsharing-dev issues](https://gitlab.com/foodsharing-dev/foodsharing/issues). This is meant for all bugs and feature requests (please, not so many!) regarding the existing [foodsharing.de](foodsharing.de) desktop website. Sensitive bugs can be marked as "confidential".

There is [GitHub foodsharing-infrastructure issues](https://github.com/foodsharing-dev/foodsharing-infrastructure). Please put issues regarding server infrastructure here! That are things like downtime, certificate problems, wants & wishes for other software, ...)

There is [GitHub foodsharing-api issues](https://github.com/foodsharing-dev/foodsharing-api/issues). Please put all issues regarding the new foodsharing-light API here!

There is [GitHub foodsharing-light issues](https://github.com/foodsharing-dev/foodsharing-light/issues). Please put all issues regarding the new foodsharing-light frontend here!
