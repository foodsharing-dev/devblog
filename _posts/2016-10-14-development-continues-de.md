---
layout: post
title:  Wie und warum geht die Entwicklung der foodsharing.de Plattform weiter?
ref:    foodsharing-dev-continues
date:   2016-10-14 01:25:00 +02:00
lang:   de
orig-lang: en
translator: fenja
author: matthias
---

Viele von euch werden sich fragen, warum bekanntgegeben wurde, dass die Entwicklung der
foodsharing Plattform eingestellt wurde, und es jetzt doch weitergeht?
Warum entwickeln wir foodsharing.de weiter, obwohl es da dieses [yunity](https://yunity.org) Projekt gab, 
das versuchte eine Nachfolgeplattform zur Verfügung zu stellen? Und dann gibt es da ja auch noch [sharecy](http://sharecy.org/)?

Lasst mich mit ein paar Worten über mich anfangen. Ich lernte foodsharing (damals noch lebensmittelretten.de) kennen,
weil ich sah, dass eine Ex-Freundin an irgendeinem Foosharing Event in Hamburg im August 2014 interessiert war.
Da ich seit etwa einem Jahr Veganer war, war ich schnell fasziniert von der Idee Lebensmittel zu retten und mit
Gleichgesinnten in meinem Umfeld zu interagieren. Also trat ich mit Raphael Wintrich, der lebensmittelretten.de
ganz alleine programmiert hat, in Kontakt, um mich auch mit meinen Fähigkeiten einbringen zu können.
Dies war etwa zur selben Zeit, als ich meinen ersten Foodsaver-Ausweis erhielt.

<!--more-->

Er war glücklich darüber, dass ich mit einstieg. Ich hatte Zeit in den Quellcode einzutauchen,
während er am Zusammenschluss von foodsharing.de und lebensmittelretten.de arbeitete. 
Ich durfte den Code zur Migration der Chatnachrichten schreiben, den wir in der Nacht vor dem
großen Relaunch am 12.12.2014 zum Laufen brachten. Um 12:12 durfte ich die Person sein,
die live die Befehle auf dem Event in Köln eintippte, um foodsharing.de in neuer Form
wiederauftreten ließ:
Die "neue" gemeinsame Plattform.

Dieser Schritt von lebensmittelretten.de zu foodsharing.de ließ leider eine Menge 
ungenutzen Code in der aktuellen Codebase zurück - ich habe gerade 8600 Zeilen an 
Code entfernt und es gibt noch mehr zu löschen - aber hatte auch ein paar neue 
Features: Beispielsweise die Nutzung von Websockets per socket.io, anstatt
Long-Polling XHR Anfragen für Nachrichten und Benachrichtigungen, die sonst nicht in der Lage 
gewesen wären, die aktuelle Serverlast zu bewältigen. Andere Veränderungen umfassen
schönes Refactoring zu Modulen sowie die Deaktivierung einiger Funktionen, die 
schwierig am Laufen zu halten waren.

Raphael und ich nahmen beide Abstand von aktivem Feature-Development. Es gab auch 
keinen sichtbaren Development-Prozess, der uns leitete, keine gute Test-Umgebung,
keine Anwendungsumgebung, nicht einmal ein einfaches Datenbank-Schema.
Zu dieser Zeit wusste ich nicht, dass dies gut mit ein paar Tagen Arbeit zu Schaffen
gewesen wäre, wie Nick später bewiesen hat. 

Da der Code nicht "schön" genug war, um neue Developer einzubringen, hatten wir nur ein paar
Leute, die vorbeikamen um sehr kleine Dinge beizutragen - aber nichts, dass die 
Plattform auf lange Zeit hin am Leben erhalten würde.

Wir gaben gewissermaßen die kostbare Software, die Raphael in Stunden um Stunden von Arbeit
aufgebaut hatte, auf - und entschieden uns in Richtung yunity weiterzumachen. Zu der Zeit - Anfang 2015 - 
hatte niemand überhaupt nur daran gedacht, dass yunity in die Richtung
gehen würde, in die es jetzt gegangen ist, da nur eine Handvoll Leute beteiligt war 
(Ihr könnt meinen Lightning Talk vom 31c3, Ende Dezember 2014 anschauen: [31c3 Lightning Talks on Congress Wiki](https://events.ccc.de/congress/2014/wiki/Lightning%3AYunity_-_A_tool_to_support_sharing_culture)).
Lest mehr auf [yunity.org](https://yunity.org) (oder besser im [Wiki] (https://yunity.atlassian.org)), um mehr über das Projekt zu erfahren.

Beim ersten yunity-Treffen - wir waren ungefähr 30 Leute mit verschiedenen Fähigkeiten,
weniger als ein Drittel dabei bei foodsharing aktiv - gab es eine Diskussion, ob wir von der foodsharing-Plattform aus (Refactoring)
oder nochmal von vorne anfangen sollten (Rebuilding). Da ich noch nie vorher an einem großen
Webprojekt mitgearbeitet hatte, also nicht wusste, dass es auch zufriedenstellend sein kann,
an einem PHP Projekt, das um eine gute Infrastruktur aufgebaut wurde, zu arbeiten, 
war ich stark voreingenommen für Rebuilding abzustimmen. Das systemische Konsensieren
ging sehr knapp für Rebuilding aus (Siehe [Aufzeichnungen von Doug](https://yunity.atlassian.net/wiki/display/YUN/The+decision+to+rewrite)),
also fingen wir an, daran zu arbeiten. Zu diesem Zeitpunkt erzählte ich offiziell allen,
dass nicht weiter an foodsharing.de gearbeitet werden wird - außer am grundsätzlichsten
Support, um die Plattform am Laufen zu halten, bis unsere yunity Software bereit ist, zu übernehmen.

<img src="/images/malo-coding.jpg">

Die Zeit danach war sehr aufregend. Wir arbeiteten an vielen schönen Orten an verschiedenen Ansätzen
und bauten viele Freundschaften und ein Team mit starken Bindungen auf, sowie eine Gemeinschaft,
die für viele Menschen, die vorbeikommen, attraktiv ist - und die meisten blieben. Nur eine Sache hat nicht
wirklich geklappt: Die Software fertig zu bekommen. Unsere Erwartungen waren hoch, unsere
Spezifikationen wuchsen, und waren trotzdem immer noch unvollständig.

> Looking at progress so far, if I estimate we have completed 4% of the MVP (as currently defined in the wiki)
> in 9 months, it will take 18 years to develop (smile)  
> --<cite>[Nick Sellen][1]</cite>

> Wenn man sich den aktuellen Fortschritt anschaut und ich abschätze, dass wir etwa 4% des Minimalprodukts (wie es gerade im Wiki spezifiziert ist)
> in 9 Monaten umgesetzt haben, wird es etwa 18 Jahre zu entwickeln brauchen (smile)
> --<cite>[Nick Sellen][1]</cite>

[1]:https://yunity.atlassian.net/wiki/display/~nicksellen/MY+BIG+THOUGHTS#MYBIGTHOUGHTS-Reflectionsonpast/present

Zu der Zeit setzten wir uns einen neuen Fokus. Die yunity-Entwickler fingen an, an mehreren kleineren
Tools zu arbeiten, statt nur an dem einen großen Monolithen. 

Im Juli hat Nick angefangen, sich den foodsharing-Code anzusehen. Am 5. August hatten wir die
Entwicklungsumgebung soweit, dass sie einfach benutzt werden kann ([f68c4eeb4](https://gitlab.com/foodsharing-dev/foodsharing/commit/f68c4eeb40777845401977f92a984ca375cf7435)) 
und am 14. August war das erste Deployment geschafft, das ein paar Environment-Veränderungen
beinhaltete, die darauf folgende Deployments möglich machten, ohne das Schlimmste
befürchten zu müssen. 

Letztes Wochenende, vom 07.10.2016 bis 09.10.2016, fand ein yunity Hackathon im [co-up](http://co-up.de/) Coworking Space in Berlin
statt, wo wir uns auch viel auf Foodsharing konzentrierten. Seitdem läuft es mit der Entwicklung.
Die meiste Zeit wird derzeit damit verbracht, Mitwirkung so einfach wie möglich zu machen.
Das wird Refactoring mit einschließen, also das Anfassen von so gut wie jeder einzelnen Zeile des Codes
in der Codebase (dort befinden sich etwa 50000 von diesen). Wir haben uns dafür entschieden,
langsam einige Kernkomponenten von [Symfony](https://symfony.com/components) mit einzubauen
und hoffentlich letztendlich eine brauchbare, komplett getestete Codebase zu haben, 
die von einem CI-System automatisch gebaut, getestet und ausgeliefert werden kann,
wenn Kernmitwirkende die Veränderungen akzeptieren. Dass die Plattform Open Source
wird, ist ein starker Wunsch der meisten Mitwirkenden, da das der Vision des Teilens
entspricht.

<img src="/images/berlin-hackathon.jpg">

Bis jetzt haben wir folgende Dinge geschafft:
* Eine Entwicklungsumgebung zu schaffen, die mittels drei Befehlen ausgecheckt, eingerichtet und ausgeführt werden kann (mittels Docker/Docker compose)
* Integration von Codeception für Acceptance, API und Unit Tests
* Lokale Ausführungen von allen Tests
* Automatische Ausführung von Tests in einem CI-System (Gitlab CI) für jeden Push
* Acceptance Tests für eine sehr kleine Auswahl an Seiten, mehr werden folgen
* Open Issue Tracker, um die Community in Fehler-Berichterstattung und Feature-Diskussionen einzubeziehen
* Ein/e Mitwirkende zu haben, der/die an Feature-Development arbeitet
* Drei Mitwirkende zu haben, die an Fehlerbehebungen, Sicherheitslücken sowie Refactoring des Codes arbeiten

Das bedeutet auch, dass wir noch einen langen Weg vor uns haben. Wie auch immer, dieser Post
sollte euch darüber informieren, was und warum ich und wir tun, auch wenn ich meine
Gründe fürs Zurückkommen noch nicht richtig ausgearbeitet habe.

Ihr könnt euch sicher sein, dass es ein starker Wille ist, nicht nur von mir, aber auch von 
anderen Mitwirkenden, die Foodsharing-Community lebendig zu halten und ihnen eine Plattform
zur Verfügung zu stellen, die ihnen nicht in die Quere kommt, wenn es in erster Linie
ums Lebensmittelretten geht. 

Die foodsharing-Community besteht aus einem liebenswerten Haufen von Menschen; ich habe es noch 
nie erlebt, so willkommen geheißen zu werden, egal wo ich hinkomme. Durch foodsharing
habe ich die Möglichkeit bekommen, bei der einzigartigen yunity-Bewegung teilzuhaben, 
die mich mit einer Menge neuer Visionen und anderen Sichtweisen auf das Leben und die Welt
ausgestattet hat. Die Erfahrung der letzten zwei Jahre hat meinen Weg für mein aktuelles
und zukünftiges Leben auf eine Weise verändert, die ich so davor niemals vorrausgesehen hätte.

Das ist mein Grund, warum ich zumindest versuchen muss, meinen Teil beizutragen, um diese
Community und Bewegung aufrechtzuerhalten.

Vielen Dank fürs Lesen!
