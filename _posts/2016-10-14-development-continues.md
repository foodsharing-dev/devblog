---
layout: post
title:  How and why does foodsharing.de development continue?
ref:    foodsharing-dev-continues
date:   2016-10-14 01:25:00 +02:00
lang:   en
orig-lang: en
author: matthias
---

A lot of you may be asking yourselves, why was it anounced that there is no more development on
the foodsharing platform when it is now continuing? Why is development on foodsharing even
continuing, although there was this [yunity](https://yunity.org) project trying to provide a replacement platform?
And then there is [sharecy](http://sharecy.org/) as well?

Let me start with some words about me. I got to know foodsharing (then still lebensmittelretten.de)
because I was seeing an ex-girlfriend interested on some foodsharing hamburg event in august 2014.
Being a vegan for about a year, I was quickly fascinated of the idea of saving food and interacting
with similar-minded people in the area, so I got into contact with Raphael Wintrich, the one who programmed
the whole platform alone, at approximately the same time when I got my first foodsaver ID card.

<!--more-->

He was happy for me stepping in and I had time to dive into the codebase while he was working on the
fusion of foodsharing.de and lebensmittelretten.de. I got to write the chat message migration code
that we ran on the night before the big relaunch at 12.12.2014. At 12:12 pm, I got to be the person
typing in the commands live at the event in cologne to let foodsharing.de reappear under a new shape:
The "new" common platform.

That step from lebensmittelretten.de to foodsharing.de unfortunately left a lot of unused code in
the current codebase - I just removed 8700 lines of code and there is more to delete - but also
had some new features, namely introducing websockets via socket.io instead of long polling XHR requests
for message and notifications, which would not be able to handle the current server load otherwise.
Other changes include some nice refactoring into modules as well disabling some functionality that was
hard to keep working.

Raphael and me both stepped back from active feature development. There was also no clear development
process guiding us, no nice test environment, no deploy environment, not even a simple database schema.
At that time, I did not know that this would have been possible to achieve nicely in some days of work
as Nick proved lately.

Without having the code "nice" for new developers to step in, we just had some people passing by to
contribute very small things - but nothing that would keep the platform alive in the long run.

We kind of gave up on the precious software Raphael has build in hours of hours of work - and decided
to proceed towards yunity. At that time - early 2015 - noone has ever thought about yunity going into
the direction it now did, as there were only a handful of people involved (You can watch my lightning
talk from the 31c3, end of december 2014: [31c3 Lightning Talks on Congress Wiki](https://events.ccc.de/congress/2014/wiki/Lightning%3AYunity_-_A_tool_to_support_sharing_culture)).
Read on at [yunity.org](https://yunity.org) (or better in [the wiki](https://yunity.atlassian.org)) to know more about that project.

At the first yunity meeting - we had approximately 30 people with different skills, less than a third
from foodsharing - there was a discussion on whether to start working on the foodsharing platform
(refactoring) or start over from scratch (rebuilding). Not having worked on a big web project
before so not knowing that contributing to a PHP project build around a nice infrastructure could
also be satisfying, I was strongly biased to vote on rebuilding. The systemic consensus was very
close in favor of rewriting (See [minutes by Doug](https://yunity.atlassian.net/wiki/display/YUN/The+decision+to+rewrite)),
so we started working on that. At that time, I was officially spreading the word that no further contributions
to foodsharing.de will be made - except the most basic support to keep it running until our yunity software
is ready to take over.

<img src="/images/malo-coding.jpg">

The time after that was very exciting. We have been working on different approaches at a lot of nice
locations, building up a lot of friends, a team with strong relationships, a community that is appealing
to a lot of people who come by - most of them stay. Just one thing did not really work out: To get the
code done. Our expectations were high, our specifications growing and still very incomplete.

> Looking at progress so far, if I estimate we have completed 4% of the MVP (as currently defined in the wiki)
> in 9 months, it will take 18 years to develop (smile)  
> --<cite>[Nick Sellen][1]</cite>

[1]:https://yunity.atlassian.net/wiki/display/~nicksellen/MY+BIG+THOUGHTS#MYBIGTHOUGHTS-Reflectionsonpast/present

At that time, we refocused. The yunity developers started working on multiple smaller tools instead of that
big monolith.

In July, Nick started to look at the foodsharing code. On August, 5th, we had the dev setup ready to be used
([f68c4eeb4](https://gitlab.com/foodsharing-dev/foodsharing/commit/f68c4eeb40777845401977f92a984ca375cf7435)) and on August, 14th
the first deployment was done that included some environment changes making following deployments possible without
having to fear the worst.

Last weekend, 07.10.2016 - 09.10.2016, there was a yunity hackathon in the [co-up](http://co-up.de/) coworking space in Berlin
where we also focussed a lot on foodsharing. Since then, development is in the flow.
Most of the time is now spent to make contribution as easy as possible. That will include refactoring, means
touching almost every single line of code in the Codebase (approx. 50000 of them are there).
We made the decision to slowly move in some core components of [symfony](https://symfony.com/components) and hopefully end
up with a workable, fully tested codebase that can be deployed from a continuous integration system automatically
when a core contributor approves the changes. Having the platform open sourced is a strong wish from most
contributors as it fits the vision of sharing.

<img src="/images/berlin-hackathon.jpg">

We achieved so far:
* Having a development environment that is checked out, setup and running with 3 commands (using docker/docker compose)
* Integration of Codeception for Acceptance, API and Unit tests
* Local execution of all tests
* Automated execution of tests in a CI system (Gitlab CI) for every push
* Acceptance tests for a very little selection of pages, more to come
* Open issue tracker to include the community in bug reporting and feature discussion
* Having one contributor working on feature development
* Having three contributors working on fixing (security) issues and refactoring the code

That also means, there is still a long way to go. Anyway, this post should let you know what and why I am and we
are doing, although I did not really elaborate my reasons for coming back.

Just be assured that it is a strong will from not only myself but also other contributors to keep the foodsharing
community alive and provide them a platform that does not get in their way when it is about saving food in the
first place.

The foodsharing community consists of a lovely bunch of people, I never experienced being that welcomed whereever I go before.
Through foodsharing, I got the possibility to participate in the unique yunity movement which provided me
with a lot of new visions and another perceptive on life and the world. The experience of the last two years
will have changed my way for my ongoing and upcoming life in a way I would never have expected the time before that.

This is my reason why I at least need to try to contribute my share to keep this community and movement going.

Thank you for reading!


