---
layout: post
title:  E-Mail-Problematik bei foodsharing.de
ref:    what-is-the-email-problem
date:   2017-12-04 20:44:00 +01:00
lang:   en
orig-lang: de
author: matthias
translator: jonathan
---

--- UPDATE: Greensta continues to support us as before and we are not necessarily looking for a new solution here. If you're still interested in helping out in this area, [feel free to read more about what we do](https://devdocs.foodsharing.network/it-tasks_EN.html) and then [just get in touch via Slack on the #foodsharing-dev channel](https://slackin.yunity.org) ---

What's going on with the emails at foodsharing?

Well, we want to send about 1 million of them a month. That's not so easy.

<!--more-->

At the moment we are sponsored by Greensta with a web package that includes emails.
There we simply set up an account through which all outgoing email traffic from @lebensmittelretten.de is handled.

A few days ago, Greensta moved its server, which I unfortunately only found out about on 3 December, when it was already too late.
Since this server move, an account must now be set up for each email address from which we want to send emails (example: berlin@lebensmittelretten.de, news@lebensmittelretten.de, ...).
Previously, this was simply possible with one account, so we could send from all email addresses on our own domain (So the account admin@lebensmittelretten.de could also send for news@lebensmittelretten.de).

This problem could possibly be solved by talking to Greensta support.

Nevertheless, it then remains that this way of sending emails is not ideal:
Our application - foodsharing - currently gets no feedback on what happens to the emails.
With the amount we send, it is important that we do not make any further delivery attempts to unreachable addresses.

The keyword here is 'transactional email':
Services like sendgrid, mailjet, sparkpost are examples here.
They are pure email delivery services that allow the application (foodsharing) to track the delivery status of the email.
In addition, they often allow easy integration of other features such as unsubscribe function for newsletters, etc.
The interfaces are also better designed to send large volumes of emails.

Foodsharing has missed the opportunity to stay up to date with emails in the last few years.
There is a lack of solutions to reduce the volume of emails (manageable notification settings, combining several messages as a "daily update" etc.), at the same time we are still at the same level in infrastructure as 4 years ago when there were only a few hundred users on the platform.

What do we need now?

* You, if you want to work on the foodsharing software and implement the problems mentioned here.
* Your contacts, if you have suggestions on how we can temporarily send one million emails per month via SMTP (incl. SPF) (from different addresses of one domain).
* Other suggestions for solutions, short or long term

As a transitional measure, I have now reactivated sending via Greensta, but only e-mails from the address "noreply@lebensmittelretten.de" are delivered.
All emails from personalised mailboxes (ambassadors/BIEB mailboxes, districts and working groups, newsletter dispatch) are not delivered and end up directly with us as "bounce" in the rubbish, so to speak.

