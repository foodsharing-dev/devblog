---
layout: post
title:  Hackweekend Januar 2022
ref:    hackweek-winter-2022
date:   2021-11-16 20:00:00 +01:00
lang:   de
orig-lang: de
author: jonathan
---



# Es ist Zeit für die nächste foodsharing Hackweek!
Es ist Zeit für das nächste foodsharing Hackweekend!

Aufgrund der Corona-Maßnahmen werden wir sie online durchführen.

**Wann - 14. Januar bis 16. Januar 2022**

Wo - Online: BigBlueButton (ohne Anmeldung möglich), Slack.

Wir werden diskutieren, programmieren, gegenseitig Vorträge anbieten, und foodsharing gemeinsam besser machen.

Man kann auch für ein paar Stunden dabei sein, es lohnt sich unabhängig von den eigenen technischen Vorkenntnissen. Das hackweekend ist sowohl für Unterstützer\*innen als auch für Entwickler\*innen.

Sei dabei!

![](/images/MG_8700.JPG)

_Matthias and Tilmann in einer früheren Hackweek_

<!--more-->

Hackweeks sind der Motor im foodsharing development: In den Programmierzeiten, die wir über die letzten Jahre hatten, haben wir es geschafft, foodsharing Open Source zu machen, Sicherheitslücken zu schließen, neue Features zu implementieren, coole neue Frameworks einzubauen und mehr. Über die letzten Hackweek(end)s wurde hier im Devblog berichtet.

Wir schaffen es auch ohne alle an einem Ort zu sein, ein Teamgefühl zu entwickeln und einander zu unterstützen. Wir haben eine ganze Woche und nehmen uns die Zeit, um uns kennenzulernen, uns über Gott und die Welt zu unterhalten, Spiele zu spielen …

Was noch mega wichtig ist, ist dass Hackweekends nicht nur für Programmierer und Nerds sind, sondern dass es soooo viele Themen gibt, über die es zu diskutieren gilt! Und wir wünschen uns mehr Kommunikation zwischen den Entwickler\*innen und der Community von foodsharing. Du könntest auch kommen und den produktiven Vibe nutzen, um an einem anderen foodsharing Projekt zu arbeiten. Wir fänden das super!

**Lasst uns gemeinsam den Code verbessern und bereichern!**

![](/images/programming.JPG)
_Lasst uns gemeinsam den Code verbessern und bereichern!_

Diesmal wird alles selbstorganisiert ablaufen, deshalb findest du hier ein Pad, das wir alle zusammen mit Inhalten füllen. Trag dich gern mit ein: https://pad.kanthaus.online/foodsharing-hackweekend_2021# (Dort gibt es später auch einen Zeitplan, welche Themen wann gemeinsam besprochen werden sollen.)

Ein weiterer Kommunikationskanal ist der #foodsharing-hackweek Channel auf https://slackin.yunity.org/ - also komm gern auch dort hin!

Bis zur Hackweek!