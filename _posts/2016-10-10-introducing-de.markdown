---
layout: post
title:  "Ich präsentiere: Der foodsharing Entwickler Blog"
ref: introducing
date: 2016-10-10 19:21:00 +0200
lang: de
orig-lang: en
translator: janina
author: nick
---

Vieles ist in letzter Zeit passiert rund um die Entwicklung der foodsharing Plattform und wir möchten
das nun auch einem breiteren Publikum zugänglich machen.

Wir schreiben aus unserer Entwickler-Perspektive, es wird also manchmal etwas
technisch werden. Nichtsdestotrotz sollte dies interessant sein für jede in 
foodsharing involvierte Person.

<!--more-->

Zunächst ein bisschen Hintergrundgeschichte.

Foodsharing wurde anfangs fast ausschließlich vom wunderbaren Raphael Wintrich
geschrieben. Er hat zwei Jahre lang all seine Liebe und Energie in den Aufbau dieser Seite
gesteckt. Die meisten Menschen würden wohl die Motivation verlieren, bevor eine
solche Mammut-Aufgabe vollendet ist.

Seitdem die Seite online ist, hat sie die foodsharing Bewegung beflügelt und den
ca. 150000 Menschen darin ermöglicht unglaubliche Mengen Essen zu retten.

<img src="/images/foodsharing-740.jpg">

Wie dem auch sei, die aktive Entwicklung hat vor ein paar Jahren ausgesetzt und
seitdem gab es nur noch ein paar Instandhaltungsarbeiten und Bugfixes. Matthias Larisch 
übernahm die Aufgabe die Seite am Laufen zu halten, und er und Michael Knoth
haben ein paar Code-Reparaturen beigesteuert.

Über das <a href="https://yunity.org">yunity</a> Projekt wurde mein Interesse geweckt zu helfen. Bei foodsharing geht es nicht darum, Katzenbilder zu teilen oder geistreiche Kommentare zu schreiben, die User sind _wirklich_ aktiv - manche gehen mehrfach die Woche außer Haus um ans andere Ende der Stadt zu radeln und Lebensmittel vor der Tonne zu retten. Diese Seite verdient mehr Liebe von Entwicklern.

Vermutlich gibt es viele Theorien darüber, warum die Entwicklung sich verlangsamt
hat, aber die einzige, die mir sinnvoll erscheint ist, dass dieser Kram _schwer_ ist.
Ich hoffe, irgendwann herauszufinden, wieso genau das so ist.
Jetzt aber geht es darum wieder Leben in den Entwicklungsprozess zu hauchen.

Ich werde weitere Beiträge darüber schreiben, wie es läuft, und erklären, welche Schritte
wir gehen und warum :)
