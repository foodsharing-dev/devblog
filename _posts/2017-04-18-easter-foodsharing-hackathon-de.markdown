---
layout: post
title:  Oster foodsharing Hackathon
ref:    Easter-foodsharing-hackathon
date:   2017-04-18 22:25:00 +02:00
lang:   de
orig-lang: en
author: nick
translator: matthias
---

Die Staubwolken unseres kleinen Hackathons am Wochenende haben sich gerade gelichtet.

Wir begrüßten die Rückkehr des ursprünglichen Entwicklers von [foodsharing.de](https://foodsharing.de), Raphael Wintrich, mit seiner erfolgreichen Bewerbung bei der [prototypefund.de](https://prototypefund.de/en/our-first-round-of-funding-goes-to/) Förderung. Die anderen Teilnehmer in unserem kleinen Team sind ich sowie Matthias Larisch.

![](/images/DSC01030.JPG)
_Raphael und Matthias schauen auf einen Computerbildschirm_

Es ist [kein Geheimnis](/2017/02/06/new-year-progress-review.html), dass der Entwicklungsfortschritt langsam war und wir uns alle manchmal fragen, warum wir überhaupt an dem Projekt weiterarbeiten, gerade wenn wir feststellen, dass _keiner von uns die Website überhaupt benutzt_ ;)

Wie dem auch sei, der Antrieb einen Hackathon durchzuführen entstand durch Raphaels Rückkehr zum Projekt und meiner Rückkehr nach Berlin nach einigen Monaten Reisen. Ich bin immernoch vollkommen überzeugt, dass es sich um eine _großartige_ Plattform mit einer großartigen Community dahinter handelt, welche ohne Zweifel mehr Aufmerksamkeit aus der Entwickler Community verdient.

<!--more-->

Angefangen haben wir damit, unsere Gedanken in einem Etherpad ([siehe Notizen, auf Englisch](/notes/pre-hackathon.txt)) niederzuschtreiben, sodass andere Online mitlesen und beitragen könnten. Als klares Konzept hat sich herausgestellt, dass wir an einer mobil-freundlichen Seite arbeiten wollten, welche den Menschen erlaubt, die Dinge zu tun, die kurzfristig ihre Aufmerksamkeit erfordern.

Das bedeutet, die nächsten Abholtermine zu sehen, zu sehen, wer mit dir zusammen abholen geht, eine Kontaktmöglichkeit zu bieten sowie Zugriff auf deine Nachrichten zu haben. Wir hatten sehr viel Erfahrung mit Zielen, welche zu groß sind, so wollten wir es diesmal realistisch halten.

Es war überraschend einfach, Entscheidungen zu treffen, wenn es erforderlich war. Ich denke, dass es uns geholen hat, nur eine kleine Gruppe zu sein, die sich schon seit mehr als zwei Jahren kennt. Wir haben alle ähnliche Gedanken darüber, unsere Fähigkeiten als Software Entwickler einzusetzen um positive Auswirkungen zu erzeugen und dabei in einer ruhigen, unterstützenden Umgebung zu sein. Wir retteten und bereiteten all unser Essen selbst zu an diesem Wochenende und haben keinen Pizzadienst in Anspruch genommen :)

![](/images/DSC01025.small.JPG)
_Wir haben viele gerettete vegane Kekse gegessen!_

Wir haben uns entschieden, eine RESTful API zu erstellen und dabei auf [Symfony 3](http://symfony.com/) und [doctrine](http://www.doctrine-project.org/) zurückzugreifen - damit haben wir Zugang zu modernen PHP Entwicklungspraktiken und die Möglichkeit, es trotzdem eventuell mit der bestehenden foodsharing Desktop Seite zu verbinden.

Für das Frontend haben wir [VueJS](https://vuejs.org/) mit dem [Quasar Framework](http://quasar-framework.org/) ausgewählt - wir waren beeindruckt von der [großen Anzahl an Komponenten](http://quasar-framework.org/components/), guter Dokumentation und guter Entwicklungserfahrung (ein einfacher `quasar` Befehl für alles was du brauchst).

Interessanterweise sind beide Frameworks großteils von jeweils einem einzelnen Entwickler erstellt - [Evan You](https://www.patreon.com/evanyou) erschafft VueJS und [Razvan Stoenescu](https://www.patreon.com/quasarframework) erstellt Quasar - trotzdem können sie von Funktionalität und Performance mit den Frameworks welche von Teams mit vielen Leuten erstellt werden mithalten oder sie sogar überbieten.

Unsere Fertigkeiten haben sich bemerkenswert ergänzt - Raphael hat die Benutzungsoberfläche erstellt (Ansichten erstellen, Design, Benutzerführung), ich habe hauptsächlich _Frontend Sachen_ gemacht (Webpack gezähmt, Projektstruktur, grundsätzlich Dinge lauffähig gemacht) und Matthias hat die REST API gebaut (mit Doctrine gekämpft, in Symfony eingetaucht, etc.). Der Fortschritt war konstant, die Motivation hoch.

Eine der größten Herausforderungen war es, die Komponenten mit der bestehenden Seite zusammenzubringen, denn es wäre sinnlos, würde das nicht unterstützt, jedoch ist die bestehende Seite nicht dafür konzipiert worden. Ich musste das Frontend dazu bringen, sich sowohl in die neue REST API einzuloggen (für den Hauptteil der Daten), als auch in die bestehende Seite (um Chatnachrichten zu schicken sowie Updates in Echtzeit auf dem Websocket zu empfangen).

Glücklicherweise hat die Arbeit, welche ich letztes Jahr gemacht habe, um foodsharing mit Docker zum laufen zu bringen, dazu beigetragen, dass eine Version von foodsharing.de ganz einfach auf meinem Laptop zu installieren war.

Es war großartig, einige der modernen PHP Tools in Benutzung zu sehen:

![](/images/nelmio.png)
_Nelmio API docs_

![](/images/symfonyprofiler.png)
_Symfony profiler_

## Also, was habt ihr dann konkret erschaffen?

Ok ok, lasst uns anschauen, was wir am Ende gemacht haben... Ich werde beim schreiben sowieso gerade müde.

Hier sind ein paar Eindrücke von unseren Mobiltelefonen - nicht alles davon funktioniert allerdings - googlemap.jpg ;)

![](/images/DSC01037+2017-04-16T13_04_17.000.JPG)

Und dann am Ende (nahezu funktionierend!):

![](/images/fsteamchat.png)<br>
_Gruppenchat, welcher auf der echten foodsharing.de Seite stattgefunden hat_

## Alles klar, dann können wir das eben live stellen, richtig?...

Oh, so einfach ist das nicht.

Wir hatten gehofft, es würde möglich sein, das am Wochenende zum Laufen zu bringen. Ab Sonntag Abend hab ich stark dazu gedrängt, nicht mehr an Funktionalität zu arbeiten (nicht einmal völlig kaputte Sachen zu reparieren). So bliebe Montag und Dienstag, um es live zu bekommen. Sollte nicht zu schwer sein, oder?

Hier stoßen wir auf die Realität, dass wir mit einer idealisierten Sicht auf die foodsharing Datenbank gearbeitet haben.

Zum Beispiel haben wir einen API Endpunkt, um die Abholungen aufzulisten sowie eine bestimmte Abholung nach ihrem Identifikator zu holen, allerdings _gibt es garkeine einzelne Tabelle, welche Abholungen speichert_, und das, was dieser am nächsten kommt, _hat nicht einmal eine Identifikator Spalte_. Ein weiteres Beispiel: Wenn ein Benutzer gelöscht wird, werden die Verknüpfungen zu diesem nicht gelöscht, es bleiben also Referenzen zu nicht existierenden Benutzern zurück. Da sind viele weitere kleine Inkonsistenzen.

Die meisten dieser Dinge sind keine Probleme in sich, allerdings mögen ORMs (Anm. d. Übersetzers: Objektrelationale Abbildung, sprich die Abbildung von Objekten auf einer relationalen Datenbank, wie wir sie in der neuen foodsharing API einsetzen möchten) die Datenbank in einer bestimmten, ordentlichen Struktur vorfinden. Wenn man sich aus diesen Konventionen bewegt, kann es sehr schnell sehr kompliziert werden.

Matthias hat sich die Nacht durch mit Doctrine herumgeschlagen, ist in Tiefen hervorgedrungen, die kein Sterblicher jemals wissen sollten. Doctrine schlug hart zurück (Um fair zu bleiben, macht es allerdings einen großartigen Job, wenn deine Datenbank seinen Annahmen und dem Verwendungszweck genügt).

![](/images/DSC01042.JPG)
_Matthias spät am Abend am Einsetzen/live-stellen der neuen Seite mit Bier_

An vielen Punkten schien es erstrebenswert aufzugeben und einfach direkte SQL Abfragen zu schreiben, allerdings gibt uns die Benutzung von Doctrine einige echte Vorteile. Schau dir diesen einfachen Code an, welcher den API Endpunkt zum Abrufen eines Benutzers anhand seiner ID definiert, mitsamt API Dokumentation:

{% highlight php %}
<?php
class UsersController extends FOSRestController
{
    /**
     * Show details on a user
     * @ApiDoc()
     * @View(statusCode=200, serializerGroups={"profile"})
     * @Get("/api/v1/users/{id}")
     */
    public function getAction(User $user)
    {
        return ['user' => $user];
    }
}
{% endhighlight %}

Trotzdem hat diesmal Doctrine den Kampf gewonnen. Wenn ein Eintrag in der Datenbank referenziert ist aber nicht existiert, ist es _nachdrücklich der Meinung_, dass dies ein Fehler ist und man so nicht weitermachen kann. Ich muss zugeben, dass das keine schwachsinnige Perspektive ist.

Der richtige Weg, mit dieser Situation nun umzugehen, ist, diese Probleme in der Datenbank selbst zu lösen. Da wir hier von Live Daten eines im Betrieb befindlichen Systems sprechen, ist dies keine Aufgabe für einen Hackathon.

## Errungenschaften

Ich bin total glücklich mit dem Gesamtergebnis. Wir haben ein sehr gutes Gruppengefühl gehabt und eine gut aussehende, (fast vollständig) funktionsfähige, mobiltaugliche Seite in nur einem verlängerten Wochenende geschaffen.

Was nun? Meine Pläne sind es, eine gute Testabdeckung im Frontend zu erreichen, die Verzeichnisstruktur klarer zu bekommen und [bestehende Probleme](https://github.com/foodsharing-dev/foodsharing-light/issues) durchzuarbeiten, weiterhin möchte ich mich darauf konzentrieren, es Entwickler-freundlich zu gestalten um es für neue und bestehende Beitragende einfach zu machen, am Projekt teilzunehmen. Darüberhinaus wäre es großartig, engere Verbindungen mit der foodsharing Community selbst aufzubauen.

Zum Schluss noch ein paar Links für dich:

- Quellcode:
  - Frontend [github.com/foodsharing-dev/foodsharing-light](https://github.com/foodsharing-dev/foodsharing-light)
  - Backend [github.com/foodsharing-dev/foodsharing-api](https://github.com/foodsharing-dev/foodsharing-api)
  - bestehende Seite [gitlab.com/foodsharing-dev](https://gitlab.com/foodsharing-dev)
- Entwickler Tools:
  - Travis CI [travis-ci.org/foodsharing-dev/foodsharing-light](https://travis-ci.org/foodsharing-dev/foodsharing-light)
  - codecov [codecov.io/gh/foodsharing-dev/foodsharing-light](https://codecov.io/gh/foodsharing-dev/foodsharing-light)
- Erreichen kannst du uns per Chat auf [Slack](https://slackin.yunity.org) (*#foodsharing-dev* channel)
