---
layout: post
title:  E-Mail-Problematik bei foodsharing.de
ref:    what-is-the-email-problem
date:   2017-12-04 20:44:00 +01:00
lang:   de
orig-lang: de
author: matthias
---

--- UPDATE: Greensta unterstützt uns weiterhin wie bisher und wir suchen hier nicht mehr unbedingt nach einer neuen Lösung. Falls du dennoch interessiert bist, in diesem Bereich mitzuhelfen, [lies gern genauer, was wir machen](https://devdocs.foodsharing.network/it-tasks.html) und dann [melde dich einfach über Slack im Kanal #foodsharing-dev](https://slackin.yunity.org) ---

Was ist da los, mit den E-Mails bei foodsharing?

Nun, wir wollen ungefähr 1 Millionen davon im Monat versenden. Das ist garnicht so einfach.

<!--more-->

Derzeit werden wir von Greensta mit einem Web-Paket gesponsort, welches auch E-Mails enthält.
Dort haben wir einfach einen Account eingerichtet, über welchen der komplette ausgehende E-Mail-Verkehr von @lebensmittelretten.de abgewickelt wird.

Nun hat Greensta vor ein paar Tagen einen Serverumzug gemacht, von dem ich leider aufgrund unglücklicher Umstände erst am 3.12. erfahren habe; als es schon zu spät war.
Seit diesem Serverumzug muss nun für jede E-Mail-Adresse, von der wir E-Mails versenden wollen, ein Konto eingerichtet sein (Beispiel: berlin@lebensmittelretten.de, news@lebensmittelretten.de, ...).
Bisher war das einfach mit einem Konto möglich, so dass wir von allen E-Mail-Adressen unserer eigenen Domain senden konnten (Also das Konto admin@lebensmittelretten.de konnte auch für news@lebensmittelretten.de versenden).

Dieses Problem könnte man durch Reden mit dem Greensta Support eventuell lösen.

Trotzdem bleibt es dann dabei, dass diese Art des E-Mail-Versands nicht ideal ist:
Unsere Anwendung - foodsharing - bekommt derzeit keine Rückmeldung darüber, was mit den E-Mails passiert.
Bei der Menge die wir versenden ist es wichtig, dass wir an nicht erreichbare Adressen möglichst keine weiteren Zustellversuche unternehmen.

Das Stichwort hier lautet `Transactional Email`:
Services wie sendgrid, mailjet, sparkpost sind hier Beispiele.
Es sind reine EMail-Versendedienstleister, welche der Anwendung (foodsharing) erlauben, den Versandstatus der E-Mail zu verfolgen.
Zudem erlauben sie häufig einfache Einbindung von weiteren Features wie Abmeldefunktion für Newsletter etc.
Auch sind die Schnittstellen besser darauf ausgelegt, große Mengen an E-Mails zu versenden.

Foodsharing hat es in den letzten Jahren verschlafen, beim Thema E-Mails up2date zu bleiben.
Es fehlen Lösungen, das E-Mail-Aufkommen zu reduzieren (überschaubare Benachrichtigungseinstellungen, Zusammenfassen mehrerer Nachrichten als "Tagesupdate" etc.), gleichzeitig sind wir in der Infrastruktur immer noch auf dem gleichen Stand wie vor 4 Jahren, als es nur wenige Hundert Benutzer auf der Plattform gab.

Was brauchen wir nun?

* Dich, wenn du Lust hast, an der foodsharing-Software mitzuarbeiten und die hier angesprochenen Probleme umzusetzen
* Deine Kontakte, wenn du Lösungsvorschläge hast, wie wir übergangsweise eine Millionen E-Mails im Monat über SMTP (inkl. SPF) versenden können (von verschiedenen Adressen einer Domain)
* Weitere Lösungsvorschläge, kurz- oder langfristig

Übergangsweise habe ich nun den Versand über Greensta reaktiviert, allerdings werden nur E-Mails von der Adresse "noreply@lebensmittelretten.de" zugestellt.
Alle E-Mails aus personalisierten Postfächern (Botschafter-/BIEB-Postfächer, Bezirke und Arbeitsgruppen, Newsletterversand) werden nicht zugestellt und landen direkt bei uns als "Bounce" quasi im Müll.
