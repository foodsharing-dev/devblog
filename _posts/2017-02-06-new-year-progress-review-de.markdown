---
layout: post
title:  Übersicht über die Entwicklungen im neuen Jahr
ref:    new-year-progress-review
date:   2017-02-06 21:46:00 +02:00
lang:   de
orig-lang: en
author: nick
translator: fenja
---

Ein neues Jahr hat begonnen und deshalb scheint es eine gute Zeit für eine kleine
Übersicht über die neuesten Entwicklungen zu sein.

Unseren Versuch, das Development von [foodsharing.de](https://foodsharing.de)
anzukurbeln, zu beobachten war faszinierend für mich.
Solche Dinge sind heikel und es gibt keine Erfolgsgarantie.

Die Anfangsbedingung war, dass das Projekt den Aufwand wert ist, eine
großartige Community von Foodsavern hat und, dass es keine brauchbaren
alternativen Software Plattformen gibt. Es ist einfach zu gut, um es aufzugeben!

Wir haben ein paar Schlüsselerfolge erzielt:
- Einrichtung einer docker-basierten Entwicklungsumgebung
- Tests
- CI-System, um die Anwendung es bei jedem Push zu bauen/zu testen
- Einen übersichtlicheren Weg für Mitwirkende, um sich anzuschließen  
([Slack Channel](https://slackin.yunity.org/), [Öffentliche Gitlab Gruppe](https://gitlab.com/foodsharing-dev/))
- Neue Mitwirkende
- 45 abgeschlossene Tickets
- 82 zusammengeführte Merge-Requests (Änderungsvorschläge)


Im Moment ist die Entwicklung allerdings wieder abgeschwächt. Das ist in Ordnung,
Projekte bewegen sich in vielen Zyklen von Aufs und Abs. Wenn wir aufgeben,
sobald die Dinge schwierig werden, können wir nicht viel erreichen.

Lasst uns in ein paar Schwierigkeiten wühlen.

<!--more-->

### Sichere Veränderungen

Es gibt viele Veränderungen, die im Schwebezustand stecken geblieben sind.
Code wurde geschrieben, Kommentare gemacht, dann… nun ja… nichts.
Zu viel davon und die Motivation ist raus.

Ein gutes Beispiel ist
[dieses hier](https://gitlab.com/foodsharing-dev/foodsharing/merge_requests/74).

Als Mitwirkende/r ist das frustrierend – warum etwas beitragen, wenn der Code am Ende nirgendwo eingesetzt wird.

Eine andere Kategorie sind die WIP (Work In Progress) Beiträge - der/die Autor*in hat es noch nicht als
fertig gekennzeichnet, aber hat vielleicht einfach aufgehört daran zu arbeiten.

Ein Beispiel für meine eigenen Erfahrungen ist das Hinzufügen von
[vue.js](https://gitlab.com/foodsharing-dev/foodsharing/merge_requests/100)
für Frontend-Darstellung. Es ist nur ein Proof of Concept und benötigt daher mehr Diskussionen.
Aber ohne jeglichen Input zum Merge-Request, ist das Projekt stehen geblieben (vielleicht
werde ich die Arbeit jedoch wieder aufnehmen!).

### Einschleppen von Programmfehlern bei Veränderung von Features

Ein paar neue Fehler wurden zur Live-Seite hinzugefügt, nachdem Veränderungen vorgenommen wurden.
Das hat zu extra Vorsicht bei zukünftigen Veränderungen geführt.

Es ist schwierig für eine/n Überprüfer*in festzustellen, ob eine Veränderung neue Fehler einführen wird, da wir geringe Test-Abdeckun sowie ein paar strukturellen Schwierigkeiten im Code (teilweise nicht so modular) haben.

### Zu viel Abhängigkeit von Matthias

Matthias hat die Plattform mehr oder weniger am Leben erhalten.
Fast alle operationellen Veränderungen oder Entwicklungsveränderungen müssen im Moment über
Matthias gehen.

Er hat das beste Wissen über die Codebase und ist die einzige Person, die derzeit Änderungen auf die Live-Seite stellen kann
(aufgrund von Datenschutzbedingten Serverzugangsbegrenzungen).

Außerdem hat er viele andere Dinge neben foodsharing.de zu tun; er ist eine extrem
nützliche Person fürs Team!

### Probleme häufen sich schneller an, als wir sie lösen können

[126 offene Probleme](https://gitlab.com/foodsharing-dev/foodsharing/issues) sind zu viele.
Ziemlich überfordernd. Wo soll man anfangen?

### Backend wurde noch nicht nach symfony verlagert

Eins der Ziele, die wir hatten, war es Backend nach symfony zu verlagern, um es modularer zu gestalten,
Unittests einfacher durchführen zu können, und um uns Zugang zum breiten Spektrum von
[symfony-Komponenten](http://symfony.com/components) zu verschaffen.

Das sollte eine Schlüsselveränderung sein, die uns andere Vorteile erschließt,
hat aber hohe Anfangskosten.

### Frontend wurde nicht modernisiert

Ich habe ein [Experiment gestartet, Frontend zu vue.js zu übertragen](https://gitlab.com/foodsharing-dev/foodsharing/merge_requests/100).
Habe aber dann aufgehört.

Falls das abgeschlossen werden sollte, macht es Frontendtests und eine schöne modulare Struktur möglich
und weckt vielleicht das Interesse bei Frontend Entwickler*innen oder Desginer*innen.

### Immer noch nicht Open Source

Die Tatsache, nicht Open Source zu sein, schafft eine Zugangsbarriere für andere Entwickler*innen
und macht es schwieriger, neue Entwickler*innen einzuführen (sie müssen den Weg zu unserem
Slack-Raum finden und Zugang beantragen).

Und warum sollte man Zeit für ein Eigentumsprojekt aufwenden? Tatsächlich bringe ich mich nur auf
der Basis ein, dass es irgendwann Open Source werden wird.

Dies hat viele Diskussionen zwischen Personen im Projekt erfordert, aber es wurde
Fortschritt erzielt! ... sollte also kommen.

### Nicht attraktiv genug für neue Entwickler*innen

Der Hackathon war großartig und hat für eine Weile eine Fülle an Aktivität ausgelöst.
Aber wir haben es nicht geschafft, neue Mitwirkende für längere Zeit im Team zu halten.

### Mangel an Entwicklungs-Ressourcen

Das ist vielleicht die einfache Ursache von allem. Es kann nicht genug betont werden,
wie wenig Ressourcen dieses Entwicklungsprojekt hat: vielleicht etwa so viel wie 0,05
Vollzeitentwickler*innen.

Es gibt immer zu viel zu tun und es ist nicht immer klar, in welcher Reihenfolge Dinge
gemacht werden sollten.

## Lösungen?

Wenn auch langsam, hat der Forschritt nicht aufgehört. Angesichts des Ressourcenmangels
müssen wir _schlau arbeiten_ und das Beste aus dem machen, was wir haben.

Der Fokus liegt für mich darauf, Schwierigkeiten für Entwickler*innen an Features zu arbeiten
zu reduzieren und Fehler zu korrigieren. Es spaltet sich in zwei Teile auf:
technischer und menschlicher Fortschritt.

Der technische Teil sollte Menschen helfen, modularen und getesteten Code zu schreiben, der
einfach gemerged werden kann. Ein paar Ziele sind:
- erneute Bemühungen symfony-Komponenten zu verwenden
- weiter an Open Sourcing arbeiten
- Frontend zu einem modernen Framework umgestalten

Der menschliche Aspekt ist aber vielleicht bedeutender.

Alle Beteiligten haben viele andere Dinge zu tun, deshalbt ist es eine Herausforderung,
die Energie/Motivation in Bewegung zu halten, vor allem in einem Remote-Team.

Ein paar Ideen:
- regelmäßige festgelegte Anrufe/Updates
- ein klarerer und öffentlich angegebener Plan
- zuerst Arbeitspläne für Personen vereinbaren - nicht einfach anfangen zu arbeiten
- regelmäßiges Issue/Merge-Anfragen-Management/Sichtung/Aufräumen
- explizite Verantwortlichkeiten

Bezüglich dem letzten Punkt habe ich überlegt, dass eine Verantwortlichkeiten-Überprüfung nützlich sein könnte;
sie würde aufzeigen, wie viele Dinge zu Matthias führen. Aber sie knüpft auch an ein paar Gedanken an,
die ich über explizite Verantwortlichkeiten hatte. Was, wenn Personen dazu aufgefordert werden würden,
verbindlich:
- sicherzustellen, dass alle Aufgaben korrekt gekennzeichnet sind?
- zu garantieren, dass alle offenen Fragen bezüglich Merge-Anfragen angesprochen werden?
- neue Mitwirkende ins Projekt einzuführen?
- 3 Merge-Anfragen pro Monat zu überprüfen?

Es bleibt mir aber immer noch ein Rätsel! Vielleicht ist darüber zu reden/schreiben schon genug,
ein bisschen Motivation zu erwecken...
