---
layout: post
title:  Hackweekend Herbst 2020 Rückblick
ref:    hackweek-autumn-2020-review
date:   2020-09-14 14:00:00 +01:00
lang:   de
orig-lang: de
author: jonathan, chris, christian-w
---



# Blogpost über das Hackweekend im Herbst 2020 - ein Rückblick
<!--more-->

[Christian-W] Es fing an mit einer lockeren Runde. Es haben sich eine muntere Truppe von 6 Entwickler\*innen zusammengefunden, die sich ein Wochenende Zeit nehmen. Gegen 18 Uhr fand die Planung der Sessions statt. Spontan wurde sich entschlossen noch eine Sentry Session am gleichen Abend anzuschließen. Für Samstag stehen als Tagesordnungspunkte: 
* 1) Stand der Programmierung eines Wahltools 
* 2) Anschauen von Merge Requests und 
* 3) Audit Protokollierung von Betriebsfunktionen auf dem Programm. 

[Chris] Wir haben einige Zeit angeregt über geplante neue Funktionen unterhalten, darunter ein System für Abstimmungen und das Protokollieren einiger Aktionen in Betrieben.

Außerdem wurde natürlich einiges an Code geschrieben und besprochen. Eine neue Funktion ist dabei direkt herausgekommen: im Forum kann nun nach dem Titel von Themen gesucht werden. 

![](https://beta.foodsharing.de/images/wallpost/medium_5f41958b314fd1.16849804.png)

Wenn ihr euch dafür interessiert bzw. generell dabei mithelfen wollt, neue Funktionen auszuprobieren, bevor sie für alle verfügbar sind: macht gern beim Beta-Testen mit!

Dafür gibt es unter https://gitlab.com/foodsharing-dev/foodsharing-beta-testing/-/wikis/how-to/Erste%20Schritte eine ausführliche Anleitung mit vielen Bildern. Oder ihr meldet euch einfach bei der AG Beta Testing. :)

[Jonathan]  Wir haben uns zu zehnt ausgetauscht, wie ein neues Meldesystem funktionieren könnte und was technisch nötig wäre.

Dann gab es noch einen Gesprächsgang zum "Foodsharing Freundeskreis" über Finanzierungsmöglichkeiten im Ehrenamt. 
Ich bin ganz beeindruckt, wie viele dieses Wochenende am Code gebastelt und an der sozialen Struktur unserer Arbeit überlegt wurde!

Wenn du mitmachen magst, findest du uns unter: slackin.yunity.org im Kanal #foodsharing-dev