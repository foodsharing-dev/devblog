---
layout: post
title:  Wiki, Mumble und Mediendatenbank auf neuem Server
ref: 2016-10-new-server
date: 2016-10-16 22:05:00 +0200
lang: de
orig-lang: de
translator: 
author: matthias
---

Guten Abend, liebe Blog-Leser!

Seit ein paar Monaten hatten wir nun noch einen Server herumliegen, auf dem
Kristijan ein paar Wiki-Versuche für die Internationalisierung vor hatte,
der aber im Endeffekt nicht genutzt wurde und somit zur Rettung frei stand.

<!--more-->

Zudem hat sich das Setup des Servers unserer [Mediendatenbank](https://media.foodsharing.de)
als etwas zu Offen erwiesen, weswegen wir diesen leider die letzte Woche ausgeschaltet
halten mussten.

Es bot sich also an, die Mediendatenbank sowieso umzuziehen. Das Wiki sowie der Mumble
Server liefen zusammen mit anderen Projekten auf einem privaten VServer von Raphael,
somit hat mich heute der Aufräum-Ehrgeiz gepackt, diese beiden auch noch umzuziehen.

Ab heute also das Wiki, Mumble und die Mediendatenbank auf einem neuen Server, intern
haben wir ihn carrot getauft (nachdem der Server der foodsharing.de Seite banana heißt).

Für euch ändert sich nichts. Die URLs sind jetzt zudem einheitlich auf foodsharing
Subdomains umgestellt und HTTPS ist aktiviert. Die alten Domains funktionieren natürlich
weiterhin, sodass eure Lesezeichen automatisch weitergeleitet werden.

* [Mediendatenbank](https://media.foodsharing.de)
* [Wiki](https://wiki.foodsharing.de)
* Mumble: mumble.foodsharing.de

