---
layout: post
title:  New contributor notes
ref:    new-contributor-notes
date:   2017-10-12 20:17:00 +02:00
lang:   de
orig-lang: en
author: matthias
---

Hey, interessierter neuer Entwickler.
Mit diesem Beitrag möchte ich dich dabei unterstützen, in die Entwicklung einzusteigen und deine dringensten Fragen beantworten.

Um mit uns zusammen zu entwickeln, tritt bitte unserem Slack-Kanal #foodsharing-dev im [yunity slack](slackin.yunity.org) bei.

<!--more-->

Es gibt <https://foodsharing.de> (als bestehende foodsharing website (existing foodsharing site) bezeichnet), welche hauptsächlich in PHP ohne Framework geschrieben ist. Wir konzentrieren uns gerade primär aufs Refactoring, um eine [moderne, testbare Codebasis](https://gitlab.com/foodsharing-dev/issues0/issues/254) zu erhalten.
Es gibt eine stetige, aber geringe Aktivität in der Entwicklung.

Wir arbeiten im [Gitlab repository](https://gitlab.com/foodsharing-dev/foodsharing), welches derzeit nicht öffentlich ist, jedoch open source werden soll, sobald wir die grundlegensten Probleme im Code behoben haben.
Wenn du mitmachen möchtest, stelle eine Zugriffsanfrage in Gitlab nachdem du dich im Slack-Channel vorgestellt hast.

Zusätzlich gibt es da [foodsharing light](beta.light.foodsharing.de) (als foodsharing light oder fslight bezeichnet), welches von Grund auf neu mit Python3/Django im Backend und Quasar/Vue.js im Frontend geschrieben wurde. Es geht hier um minimale, dafür aber mobil nutzbare Features.
Im Moment arbeitet hier niemand dran, das kann sich aber jederzeit wieder ändern.
Wir verwalten dies im [backend Github repository](https://github.com/foodsharing-dev/foodsharing-django-api) sowie im [frontend Github repository](https://github.com/foodsharing-dev/foodsharing-light).

Beide Beta-Seiten <https://beta.light.foodsharing.de> und <https://beta.foodsharing.de> werden automatisch von den jeweiligen master-Branches deployed, also wollen wir master immer lauffähig halten.
Für die bestehende foodsharing Website gibt es eine ständig weiter wachsende Testabdeckung, hauptsächlich durch Integrations/Akzeptanztests mittels Codeception und Selenium.

Eine Entwicklerversion der Seiten mit Testdaten und der Testumgebung kann sehr einfach installiert werden. Sie läuft am einfachsten auf Linux-Umgebungen, ist aber mit etwas Arbeit auch auf Windows und Mac benutzbar.

Für Patches bzw. Weiterentwicklungen folgen wir dem Merge Request/Review Arbeitsprinzip und möchten bestimmte Coding Standards einhalten.
Siehe hierfür die jeweilige Contributors Readme.

Insgesamt kommunizieren und entwickeln wir bevorzugt auf Englisch, an der Sprache sollte deine Mitarbeit allerdings nicht scheitern :-)

Für eine mögliche erste Aufgabe, lies weiter unter [Wir brauchen deine Hilfe]({{ site.baseurl }}{% post_url 2017-10-15-we-need-your-help %}).
