---
layout: post
title:  Zwischenstand der Android App
ref:    android-app
date:   2019-09-01 12:00:00 +02:00
lang:   de
orig-lang: de
author: jo
---


# **Projektziel**

Wir hatten uns überlegt, dass wir eine App brauchen, um die Handhabbarkeit von Essenskörben für alle Benutzer*innen von www.foodsharing.de zu verbessern.
Langfristig sollen auf diese Basis alle foodsaver-Funktionen aufgebaut werden, ohne die Webseite zu ersetzen.
Vorteile einer Smartphone-App sind die vorhandenen "Sensoren" wie Kamera, GPS und Ortung.
Dem gegenüber nachrangig und doch im Blick ist die Entwicklung der Apple (iOS) App.  

![](/images/02_conversations.png)
_So sieht der Chat derzeit aus_

<!--more-->

# **Projektverlauf**
Das Projekt begann vor einem Jahr mit starker Unterstützung von Nick Sellen. 
Richtig Fahrt aufgenommen hat es dann erst im Oktober 2018, als ein weiterer Programmierer dazugekommen ist. Die gegenseitige Motivation trug Früchte. Die Chat-Funktion entwickelte sich zum gut funktionierenden Kern der App.
Zur Hackweek im Februar gab Nick die App in die Hände des Teams. Dazu gehören Alex, David, Janosch, Lukas, Diego, Simon, WUUUGI, und ich. Wir bauten die bereits angedachten Funktionen weiter und veröffentlichten am 20.04.2019 eine ["Public Beta" (öffentlichen Testversion) im **"Google Play Store"](https://play.google.com/store/apps/details?id=de.foodsharing.app).

Am 16.08.2019, also zum [foodsharing Festival](https://www.foodsharing-festival.org/), erreichte der Downloadzähler den 10.000ten Download (auf unterschiedliche Geräteaccounts). 
Ein kleines aber sehr aktives Team werkelt nun täglich an Verbesserungen: Von Projektbeginn bis heute wurden 516 Änderungen (commits) gemacht. 

Der ersten veröffentlichten Beta **Version 0.1.0 vom April 2019** sind bis dato vier weitere Versionen gefolgt, so dass wir im August bei **Version 0.4.0** angekommen sind. 
Bisher haben wir einen Update Zyklus von etwa 45 Tagen, wobei wir kürzere Zyklen anpeilen.

Aktuell gibt es ca. **85 Installationen am Tag**, der Kreis der Nutzer wächst also schnell. Dabei haben wir auf ausdrückliche Werbung innerhalb der Community bisher verzichtet, um die Supportanfragen gering zu halten. (Wer im Support helfen möchte, findet unten unsere Kontaktdaten.)
Aktuell wird etwa alle 3 Tage intern eine neue Version einer internen "foodsharing Beta" App veröffentlicht. Dafür sind nach einer Anfrage über die genannten Links noch ca. 80 "Testplätze" frei. In Zusammenarbeit in einer Slack Chat Gruppe wird hier sogenannten "Testaufrufen" gefolgt, um die Tester auf Detailänderungen hinzuweisen und Hand in Hand zu arbeiten.

# **Aufruf zur Unterstützung**

Vielleicht bist ja du, werte lesende Person, ein begeisterter Nutzer der App oder sogar eine Programmiererin; ein lernfähiger Mensch mit etwas Zeit und dem Willen, dich einzubringen. Vielleicht hast du sogar Lust, bei der Apple (iOS) App Entwicklung zu unterstützen?
Dann schau doch mal in unseren [Aufruf zur Mitarbeit](https://devdocs.foodsharing.network/it-tasks.html) und melde dich bei uns (zum Beispiel per Mail: it@foodsharing.network). 
Wir freuen uns auf dich.
