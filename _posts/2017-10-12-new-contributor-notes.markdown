---
layout: post
title:  New contributor notes
ref:    new-contributor-notes
date:   2017-10-12 20:17:00 +02:00
lang:   en
orig-lang: en
author: matthias
---

Hi there, new interested developer.
This post should help you to get started in foodsharing development and answer your most urgent questions :-)

For general participation in development, please join our slack channel #foodsharing-dev on the [yunity slack](slackin.yunity.org).

<!--more-->

So, there is <https://foodsharing.de> (referred to as 'the existing foodsharing site') which is mainly written in PHP without a framework. We are currently focusing on refactoring, to get to a [modern, testable codebase](https://gitlab.com/foodsharing-dev/issues0/issues/254).
There is active, but slow development.
We have the [gitlab repository](https://gitlab.com/foodsharing-dev/foodsharing) which is currently still private but will be open sourced as soon as we addressed the major known issues.
Please feel free to request access to that repository after you joined the slack channel and introduced yourself.

Additionally, there is [foodsharing light](beta.light.foodsharing.de) (referenced as 'foodsharing light' or 'fslight') which is a rewrite in Python3/Django as well as Quasar/Vue.js with minimal features and focus on mobile.
It is currently not in active development but that can change any time.
It is managed in a [backend github repository](https://github.com/foodsharing-dev/foodsharing-django-api) as well as in a [frontend github repository](https://github.com/foodsharing-dev/foodsharing-light).

Both beta pages <https://beta.light.foodsharing.de> as well as <https://beta.foodsharing.de> are auto-deployed from the master branch, so we want to keep master always running.
For the existing foodsharing site, there is growing test coverage mostly through codeception acceptance tests run in selenium.

A development version with some sample data as well as the test environment are very easy to set up and well supported on any linux system (while you might need some fiddling on windows or mac).

For code contributions, we want to follow a merge request/review workflow and stick to certain coding/formatting standards.
Please see the contributors readme in the appropriate repository for details.

Since foodsharing._de_ has a German-speaking community you may face some German words here and there, don't hesitate to request translations if you feel that it is needed!

Looking for a possible first task? Read on in [We need your help]({{ site.baseurl }}{% post_url 2017-10-15-we-need-your-help %}).
