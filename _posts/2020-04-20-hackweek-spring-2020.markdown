---
layout: post
title:  Hackweek Frühling 2020
ref:    hackweek-spring-2020
date:   2020-04-20 17:00:00 +01:00
lang:   de
orig-lang: de
author: jonathan
---



# Es ist Zeit für die nächste foodsharing Hackweek!
Aufgrund der Corona-Maßnahmen werden wir sie online durchführen. 

**Wann - 25. April bis 3. Mai 2020**

Wo - Online: Jitsi, Slack, Mumble, ...

Wir werden diskutieren, programmieren, gegenseitig Vorträge anbieten, und foodsharing gemeinsam besser machen.

Man kann auch für ein paar Stunden dabei sein, es lohnt sich unabhängig von den eigenen technischen Vorkenntnissen. Die hackweek ist sowohl für Unterstützer\*innen als auch für Entwickler\*innen.

**Sei dabei!**

![](/images/MG_8700.JPG)

_Matthias and Tilmann in einer früheren Hackweek_

<!--more-->

Hackweeks sind der Motor im foodsharing development: In den Hackweeks, die wir über die letzten Jahre hatten, haben wir es geschafft foodsharing Open Source zu machen, Sicherheitslücken zu schließen, neue Features zu implementieren, coole neue Frameworks einzubauen und mehr. Über die letzten Hackweeks wurde hier im Devblog berichtet.

Es ist außerdem toll mal zusammenzukommen! An foodsharing zu arbeiten ist meistens eine recht einsame Angelegenheit, aber wir schaffen es auch ohne alle an einem Ort zu sein, ein Teamgefühl zu entwickeln und einander zu unterstützen. Wir haben eine ganze Woche und nehmen uns die Zeit, um uns kennenzulernen, uns über Gott und die Welt zu unterhalten, Spiele zu spielen...

Was noch mega wichtig ist, ist dass Hackweeks nicht nur für Programmierer und Nerds sind, sondern dass es soooo viele Themen gibt, über die es zu diskutieren gilt! Und wir wünschen uns mehr Kommunikation zwischen den Entwickler\*innen und der Community von foodsharing. Du könntest auch kommen und den produktiven Vibe nutzen, um an einem anderen foodsharing Projekt zu arbeiten. Wir fänden das super!

![](/images/programming.JPG)
_Lasst uns gemeinsam den Code verbessern und bereichern!_

# Einige grundlegende Details

Verpass nicht die Sitzungen zu Themen wie "Burnout-Prävention", "Was passiert, wenn foodsharing ausfällt", "die lokale foodsharing Umgebung für absolute Anfänger", "Roadmap der nativen Apps", "Aktuelle Forschung zur Mensch-Lebensmittel-Interaktion" und viele andere. (Wir haben sogar eine Pen&Paper-Runde geplant).

Diesmal wird alles selbstorganisiert ablaufen, deshalb findest du hier ein Pad, das wir alle zusammen mit Inhalten füllen. Trag dich gern mit ein: **[https://codi.kanthaus.online/foodsharing-hackweek?view](https://codi.kanthaus.online/foodsharing-hackweek?view)**
(Dort gibt es auch einen Zeitplan, welche Themen wann gemeinsam besprochen werden sollen.)

Ein weiterer Kommunikationskanal ist der #foodsharing-hackweek Channel auf https://slackin.yunity.org/ - also komm gern auch dort hin!

Bis zur Hackweek!