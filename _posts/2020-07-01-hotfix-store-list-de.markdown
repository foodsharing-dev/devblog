---
layout: post
title:  Was in der letzten Zeit geschah... Hotfix, neue Teamliste, Internationalisierung
ref:    hotfix-store-list
date:   2020-07-01 12:00:00 +02:00
lang:   de
orig-lang: de
author: caluera
---



**Hotfix**

Mitte Juni gab es einen Hotfix. Dabei wurden mehrere Probleme behoben, die teilweise beim letzten Release mit reinkamen oder schon länger bestanden und immer dringlicher wurden.
Da wir ein relativ hohes E-Mail-Aufkommen im Vergleich zu der uns möglichen Sendefrequenz haben, dauert es immer eine Weile, bis E-Mails ankommen.
Dadurch gab es recht viele Supportanfragen, dass E-Mails zu vergessenen Passwörtern, zur Änderung von E-Mail-Adressen und zur Registrierung nicht ankämen. Diese haben nun eine höhere Priorität, sodass diese nun schneller als andere E-Mail-Benachrichtigungen ankommen.

Wenn eine E-Mail von foodsharing nicht zugestellt werden kann, kommt die zugehörige Adresse auf die Bounce Liste. An E-Mail-Adressen auf dieser Liste werden keine Mails mehr versandt. Seit dem Hotfix wird nun eine Warnung auf dem Profil angezeigt, wenn dies so ist, mit einer Erklärung, wie das geändert werden kann.
Es gibt auch beim Support jetzt einen [Artikel](https://foodsharing.freshdesk.com/support/solutions/articles/77000299947-e-mail-sperre-im-profil), in dem das ganze noch mal genauer erläutert ist.

Es kam zu einem Bug, wo das bei der Registrierung angegebene Geschlecht falsch abgespeichert wurde. Dieser ist nun behoben. 

**Neue Teamliste**

In den Betrieben ist eine neue Teamliste in Arbeit. Diese wurde in Vue.js neuimplementiert. Damit ist die Liste nun deutlich mobile-freundlicher. Zuvor war es so, dass es beim Klicken auf eine Person in der Liste eine direkte Weiterleitung gab, sodass die Nummer gewählt wurde. Wollte man die anderen Menüoptionen nutzen, musste man erst zurück zum Browser.

Desweiteren sind nun die Betriebsverantwortlichen immer oben in der Liste, selbst wenn sie eine Schlafmütze aufhaben. Damit sind diese deutlich schneller in der Liste auffindbar. Außerdem wird nicht mehr farblich markiert, ob eine Person Botschafter:in ist oder war, für das vorher die Markierung die selbe war. Außerdem können solche Kennzeichnung Hierarchien verstärken, obwohl diese im Kontext der Abholungen keine Rolle spielen sollten. 

**Internationalisierung**

Es gibt die ersten Übersetzungen ins Englische!

Bislang gab es ein selbstgebautes System, welches Texte in die Webseite einband. 
Durch Anpassung an Standards wird es für neue Entwickler nun einfacher, sich einzufinden, und es ist weniger Aufwand, einen Umschaltbutton für andere Sprachen einzurichten.

Technisch bedeutet dies, dass unser Projekt weiter an die Symfonystandardstruktur angepasst wurde. 
Das beinhaltet eine Sitzungsvariable, die die aktuelle Sprache speichert, und die von Symfony vorgegebene Dateibenennung.
Dadurch brauchen wir nun keine eigenen Übersetzunghelfer und -validierer mehr, sondern können direkt Symfony nutzen.
Stück für Stück werden weiterhin die alten Übersetzungen angepasst. 