---
layout: post
title:  Wir brauchen deine Hilfe!
ref:    we-need-your-help
date:   2017-10-15 16:48:00 +02:00
lang:   de
orig-lang: en
author: nick
translator: matthias
---

Eine nette Agentur hat eine Kampagne für foodsharing entwickelt. Damit können
wir den Aufruf gegen die Lebensmittelverschwendung nun über soziale Netzwerke,
Werbetafeln und Taschen verbreiten!
Wie cool!

Das Problem ist, dass die Kampagne nicht das ganze Konzept von foodsharing
erklären wird, aber die Menschen direkt auf die Homepage leitet, welche
derzeit keinen guten Überblick darüber gibt, wie genau das bei foodsharing
eigentlich alles funktioniert.

Es gibt bereits ein [neues Konzept für eine Startseite](/images/foodsharing_startseite.pdf),
die Aufgabe ist nun, dies in die
[foodsharing.de](https://foodsharing.de) Webseite zu integrieren.

<!--more-->

Du würdest denken, dass wir bereits in unserem Entwicklungsteam in der Lage sein
sollten, dies zu tun, aber dies ist nicht der Fall - genau genommen besteht
das "_Entwicklungsteam_" nur aus ein ganz paar Menschen die ein paar Stunden
ihrer Zeit hier und da zur Verfügung stellen (Das ist schon ziemlich großartig,
dass jeder Entwickler _Zehntausende_ Nutzer unterstützen kann, mit nur ein paar
Stunden pro Woche!).

Folgende Fähigkeiten solltest du mitbringen:

1. unsere docker-compose basierte Entwicklungsumgebung aufsetzen
  - (funktioniert am besten unter Linux, dann osx, nicht so gut unter Windows, ne VM
  tuts aber auch)
2. genügend html/css/js/php verstehen um den Inhalt und obendrauf soviel Design wie du magst einzubauen
3. die bestehenden (teilweise völlig veralteten) js/css bibliotheken und themes zu verwenden um den Einfluss auf den Rest der Seite gering zu halten
4. Rückmeldungen von verschiedenen Leuten anzunehmen (Entwickler, Design, Vorstand, etc.)
5. Die Aufgabe so zu erledigen, dass wir sie im Dezember live schalten können

Ohne die neue Startseite kann die Kampagne nicht gestartet werden, das würde uns sehr traurig machen :(

Wenn du glaubst, uns helfen zu können, komm bitte in den 
[**#foodsharing-dev** Kanal im Slack](https://yunity.slack.com) [Einladung](https://slackin.yunity.org) und sag Hallo!
