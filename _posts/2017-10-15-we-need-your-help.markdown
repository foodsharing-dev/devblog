---
layout: post
title:  We need your help!
ref:    we-need-your-help
date:   2017-10-15 16:48:00 +02:00
lang:   en
orig-lang: en
author: nick
---

A nice agency has created a campaign for foodsharing which means we can spread
the message about food waste prevention via social media, posters, and bags!
How cool!

The problem is that the campaign will not really explain the whole concept of
foodsharing, but just direct people to the homepage, which currently is really
lacking a general overview of how it all works.

We actually have a [new homepage concept](/images/foodsharing_startseite.pdf)
ready, but the task is to incorporate it into the existing
[foodsharing.de](https://foodsharing.de) homepage.

<!--more-->

You might think we already have the ability to do this within our existing
development team, but we do not - actually the "_development team_" consists of
a few people that volunteer a few hours of their time here and there (which is
  quite amazing when you think of it, each developer is supporting
  _tens of thousands_ of users from just a few hours per week).

You should have the ability to:

1. setup our docker-compose based development environment
  - (works best on linux, then osx, not so well on windows but works in a vm)
2. understand enough html/css/js/php to incorporate the content, and as much design as you feel able to
3. use our existing (sometimes quite dated...) js/css libraries and theming to minimise impact on the rest of the site
4. accept feedback from various people with opinions! (dev/design/board/etc)
5. get it ready for going live by early December

Without this the campaign cannot go ahead, and we would be very sad :(

If you think you can help please join the
[**#foodsharing-dev** channel in slack](https://yunity.slack.com) and say hi!
