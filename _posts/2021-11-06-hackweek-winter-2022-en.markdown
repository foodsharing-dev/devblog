---
layout: post
title:  hackweek january 2022
ref:    hackweek-winter-2022
date:   2020-04-20 17:00:00 +01:00
lang:   en
orig-lang: de
author: jonathan
translator: jonathan
---
​





xxxxxxxxxx

​
# It's time for the next foodsharing Hackweek!
Because of the Corona measures, we'll be meeting online. 

**When - April 25th to May 3rd, 2020**

Where - Online: Jitsi, Slack, Mumble...

We will discuss, program, give lectures to each other, and make foodsharing better together.

You can also participate for a few hours, it is worth it regardless of your own technical knowledge. The Hackweek is for both supporters and developers.

**Let's do this.**
​
![](/images/MG_8700.JPG)
_Matthias and Tilmann in a former Hackweek_

<!--more-->
​
Hackweeks are the driving force in foodsharing development: In the Hackweeks we've had over the last few years, we've managed to make foodsharing open source, we closed security holes, implemented new features, added cool new frameworks, and more. The last Hackweeks have been covered here on the Devblog.

It's also great to get together! Working on foodsharing is a lonely business most of the time but we can manage without everyone being in one place: developing a team spirit and supporting each other. We have a whole week and take the time to get to know each other, talk about everything, play games...

What is also very important is that Hackweeks are not only for programmers and nerds but that there are soooo many topics to discuss! And we would like to see more communication between developers and the foodsharing community. You could also come and use the productive vibe to work on another foodsharing project. We would love that!

![](/images/programming.JPG)
_Let us together improve and enrich the code!_

# Some basic details

Don't miss out on sessions about "burnout prevention", "What happens if foodsharing is down?", "Run foodsharing locally for total beginners", "improving the beta testing workflow", "Roadmap of the native Apps",  "Current research on Human-Food-Interaction" and many more. (We even have a Pen&Paper-Round planned.)

This time everything will be self-organized, so you'll find a pad here. Everyone can fill missing content. So: Feel free to sign up: **[https://codi.kanthaus.online/foodsharing-hackweek?view](https://codi.kanthaus.online/foodsharing-hackweek?view)**


(There is also a timetable, which topics should be discussed together and when.)

Another communication channel is the #foodsharing-hackweek channel at https://slackin.yunity.org/ - so feel free to come there too!

See you at the Hackweek!