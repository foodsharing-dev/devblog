---
layout: post
title:  Hackweek Frühling 2020 Rückblick
ref:    hackweek-spring-2020-review
date:   2020-05-22 13:00:00 +01:00
lang:   en
orig-lang: de
author: felix, amazing_k, caluera, moffer, chris
translator: jonathan
---



# Blog post about the Hackweek May 2020 - a review

[Felix] Before Hackweek, I wasn't sure if I was going to participate, so unfortunately I didn't engage in the preparation. This preparation started quite slow, but in the time before the Hackweek the program filled up with topics and initiated sessions. For me as a still-newcomer these gave the opportunity to listen, ask questions and learn.

[Kristijan]
The Hackweek is a joyful event for me to experience many of the active people live. Unfortunately Corona made this difficult. So it was all the more pleasant that there was an "online Hackweek". Sessions were held in videoconferences, so at least we could see each other and contributed to a group feeling and a "we". A nice and productive atmosphere was created and it was valuable to get feedback and new ideas for implementation. There were also funny sessions on actually serious topics, which made us loosen up. 

[Caluera]
In the Hackweek we got a lot done, especially things that would otherwise get lost in everyday life. We also talked a lot about structures and made plans how we will deal with some topics. 

[moffer]
The Hackweek has made a lot of progress, for me especially social aspects: Talking live with some people you only deal with through Gitlab or Slack. I also found the whole atmosphere positive. There was a lot of discussion, so there wasn't much time for hacking - unfortunately for me the time was limited. However, I can imagine that such an online format could take place more often, since the orga effort (and possibly financial expenditure) is manageable. I would welcome that.

[Chris]
I was particularly enthusiastic about the sense of community despite the physical distance :) All in all, many exciting insights into very different areas, productive discussions and a user-oriented exchange, plus a flexible organization with more structure exactly where it was needed - I would definitely be happy about a continuation soon and hope for many new faces as well.  
So, you readers out there: next time, just drop in without obligation, drop by at some of the sessions and maybe get directly involved. Hackweeks are a great opportunity to get to know the team and the way they work - also and especially if you don't do any programming yourself!
