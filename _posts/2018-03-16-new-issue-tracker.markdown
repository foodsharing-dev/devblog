---
layout: post
title:  New issue tracker
ref:    new-issue-tracker
date:   2018-03-18 22:50:00 +01:00
lang:   en
orig-lang: en
author: matthias
---

# Issue tracker now in foodsharing project

[Peter](https://gitlab.com/peter.toennies) noticed, that gitlab supports private code with public issues. So here we go, we switched our [foodsharing project](https://gitlab.com/foodsharing-dev/foodsharing) to public and restricted the code access to accepted members.

Please use the [issue tracker](https://gitlab.com/foodsharing-dev/foodsharing/issues) inside now.
