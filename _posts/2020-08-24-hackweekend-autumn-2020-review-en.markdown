---
layout: post
title:  Hackweekend Autumn 2020 Review
ref:    hackweek-autumn-2020-review
date:   2020-09-14 14:00:00 +01:00
lang:   en
orig-lang: de
author: chris, christian-w, jonathan
translator: jonathan
---



# Blog post about the Hackweekend August 2020 - a review
<!--more-->

[Christian-W] It began with a relaxed round. A cheerful group of 6 developers took the time to spend a weekend together. Around 6 pm the planning of the sessions took place. Spontaneously they decided to join another Sentry session the same evening. The agenda for Saturday was as follows: 
* 1) Status of the programming of a voting tool 
* 2) View Merge Requests and 
* 3) Audit Logging of operating functions on the program. 

[Chris] We have spent some time discussing planned new features, including a voting system and logging some of the actions in plants.

Of course we also wrote and discussed some code. One new feature came out directly: in the forum you can now search for the title of topics. 

![](https://beta.foodsharing.de/images/wallpost/medium_5f41958b314fd1.16849804.png)

If you are interested or generally want to help to try out new features before they are available for everyone: feel free to join the beta testing team!

You can find a detailed manual with many pictures at https://gitlab.com/foodsharing-dev/foodsharing-beta-testing/-/wikis/how-to/Erste%20Schritte. Or simply contact the AG Beta Testing. :)

[Jonathan] There were ten of us who discussed how a new reporting system could work and what would be technically necessary.

Then there was a discussion with the "Foodsharing Freundeskreis" about financing possibilities in volunteer work. 
I am very impressed how many of us worked on the code this weekend and thought about the social structure of our work!

If you want to participate, you can find us at: slackin.yunity.org in the channel #foodsharing-dev
