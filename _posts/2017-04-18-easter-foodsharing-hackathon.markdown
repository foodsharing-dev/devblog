---
layout: post
title:  Easter foodsharing hackathon
ref:    Easter-foodsharing-hackathon
date:   2017-04-18 22:25:00 +02:00
lang:   en
orig-lang: en
author: nick
---

The dust has just settled from our small hackathon this weekend.

We welcomed the return of the original developer of [foodsharing.de](https://foodsharing.de) Raphael Wintrich having successfully applied for [prototypefund.de](https://prototypefund.de/en/our-first-round-of-funding-goes-to/) funding. The rest of the small team being me and Matthias Larisch.

![](/images/DSC01030.JPG)
_Raphael and Matthias stare at a computer screen_

It's [no secret](/2017/02/06/new-year-progress-review.html) that development progress has been slow and we all sometimes wonder why we continue on the project, especially when we realised _none of us actually use the website_ ;)

However, the motivation to hold a hackathon came from Raphaels return to the project and my return to Berlin after some months travelling. I am still fully convinced this is a _great_ platform, with a great community behind it, and without a doubt deserves more attention from the developer community.

<!--more-->

We started by exploring our thoughts in an etherpad ([see notes](/notes/pre-hackathon.txt)) so that others online could see and contribute. What emerged as a clear concept was to build a mobile-friendly site that allows people to do the things that need their attention now.

This means seeing when your next pickup is, who is doing it with you,  providing a way to contact them, and being able to read and reply to your chats. We had a lot of experience with having goals that are too big so wanted to keep it realistic.

It was surprisingly easy to come to decisions when we needed to, I think it really helped being a small team that have known each other for at least 2 years. We all have very similar feelings about wanting to use our software development skills to support positive causes in a calm, supportive atmosphere. We saved and cooked all our food for the weekend, no takeaway pizzas here :)

![](/images/DSC01025.small.JPG)
_We ate a lot of saved vegan biscuits!_

We decided to built a RESTful API using [Symfony 3](http://symfony.com/) and [doctrine](http://www.doctrine-project.org/) - this would allow us to use modern PHP coding practises yet eventually combine it with the existing codebase.

For the frontend we chose [VueJS](https://vuejs.org/) with the [Quasar Framework](http://quasar-framework.org/) - we were impressed with [large number of components](http://quasar-framework.org/components/), good documentation, and simple developer experience (simple `quasar` command for everything you need).

Interestingly both frameworks are largely built by a single developer each - [Evan You](https://www.patreon.com/evanyou) creates VueJS and [Razvan Stoenescu](https://www.patreon.com/quasarframework) creates Quasar - yet they offer functionality and performance on par (or often better) than frameworks made by large teams of people.

Our skills were remarkably complementary - Raphael built the user interface (defining views, theming, user experience), I mostly did _frontend stuff_ (taming webpack, project structure, generally _making shit work_), and Matthias built the RESTful API (battling doctrine, diving into Symfony, etc). Progress was steady, and motivation was high.

One of the biggest challenges was getting it to talk to the existing site, it would be no good if it couldn't do that, but it wasn't designed for this purpose. I had to get the new frontend to log into both the new RESTful API (for most of the data) and the existing site (for posting chat messages and receiving updates over websocket).

Fortunately the work I did last year to get the foodsharing code working with docker meant it was easy enough to get a version of foodsharing.de running on my laptop to use.

It was great to see some of the modern PHP tooling in use:

![](/images/nelmio.png)
_Nelmio API docs_

![](/images/symfonyprofiler.png)
_Symfony profiler_

## So, what did you actually make?

Ok ok, lets show you what we came up with... I'm getting tired of writing this now too.

So, here are a few general views on our mobile phones - not all of this is actually functional though - googlemap.jpg ;)

![](/images/DSC01037+2017-04-16T13_04_17.000.JPG)

And then by the end (and pretty much working!):

![](/images/fsteamchat.png)<br>
_Group chat that actually happened on foodsharing.de_

## So, we'll just put it live now right...

Ah, not so easy.

We had hoped it would be possible to get it up and running over the weekend. I pushed heavily to stop working on features (or event fixing blatantly broken stuff) by Sunday night. We had Monday and some of Tuesday to get it live. Can't be too hard right?

Here is where we face up to the reality we are building an idealized view of the foodsharing database.

For example, we have a pickups API endpoint where you can list pickups and fetch by id, but _there is no pickups table_, and the closest thing to it _doesn't have an id column_. Another example, when a user is deleted the associations for that user are not, it leaves behind references to non-existent users. There are many other little inconsistencies.

Most of those things are not problems in themselves, but ORMs like the database to be in a certain orderly structure. If you move outside of those conventions then it can get very complicated very quickly.

Matthias battled doctrine hard through the night, exploring depths no mortal should ever need to know. Doctrine fought back hard (to be fair doctrine does a great job if your database fits the assumptions and use cases).

![](/images/DSC01042.JPG)
_Matthias late night beer-deploying the new site_

At many points it seemed worth giving up and writing raw SQL queries, but using doctrine gives some real advantages. Look at the simplicity of this code that defines an API endpoint for getting a user by id, including all the API documentation:

{% highlight php %}
<?php
class UsersController extends FOSRestController
{
    /**
     * Show details on a user
     * @ApiDoc()
     * @View(statusCode=200, serializerGroups={"profile"})
     * @Get("/api/v1/users/{id}")
     */
    public function getAction(User $user)
    {
        return ['user' => $user];
    }
}
{% endhighlight %}

However this time the battle was won by doctrine. If an entry in the database is referenced but does not exist it is _really insistent_ that this is an error and you cannot continue. I admit it is not an unreasonable perspective.

The right way to deal with this now is to fix these kind of issues in the database itself. As this impacts live production data though, it's not a task for a hackathon.

## Achievements

I am super happy with the overall outcome. We got a really good group feeling, and a great looking [mostly] functional mobile-friendly site in just an extended weekend.

What next? My plans are to get good test coverage in the frontend, rejig the directory structure for clarity, and work through [the issues](https://github.com/foodsharing-dev/foodsharing-light/issues), beyond that I want to focus on getting it developer-friendly to make it easy for new and existing contributors to join the project, and beyond that it would be great to build stronger connections with the foodsharing community itself.

And finally, some links for you!

- source code:
  - frontend [github.com/foodsharing-dev/foodsharing-light](https://github.com/foodsharing-dev/foodsharing-light)
  - backend [github.com/foodsharing-dev/foodsharing-api](https://github.com/foodsharing-dev/foodsharing-api)
  - existing site [gitlab.com/foodsharing-dev](https://gitlab.com/foodsharing-dev)
- dev tooling:
  - Travis CI [travis-ci.org/foodsharing-dev/foodsharing-light](https://travis-ci.org/foodsharing-dev/foodsharing-light)
  - codecov [codecov.io/gh/foodsharing-dev/foodsharing-light](https://codecov.io/gh/foodsharing-dev/foodsharing-light)
- you can chat to us in [slack](https://slackin.yunity.org) (*#foodsharing-dev* channel)