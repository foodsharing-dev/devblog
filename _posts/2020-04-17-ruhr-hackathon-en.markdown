---
layout: post
title:  Programmers at work in the Pott
ref:    ruhr-hackathon
date:   2020-04-17 04:10:00 +01:00
lang:   en
orig-lang: de
author: 
- caluera
- christian-w
- ctwx
- fridtjof
translator: jonathan
---
​
​
​
On 15.02.2020, a few people from the foodsharing IT department met in Bochum to continue working on the foodsharing website.
Here are reports from some of the people present:
​
<!--more-->
​

**Caluera**: 
To make the event known, I have created a North Rhine-Westphalia-wide appointment on the foodsharing website, so that perhaps people who have not yet participated in the foodsharing programming can join in. I also found a few people who were interested in the event, but most of them didn't have the time. 
I also contacted the Social Media AG, who published an appeal for our meeting on Twitter. 
We met at the hackerspace [das Labor](https://wiki.das-labor.org/w/LABOR_Wiki) in Bochum. I went to their plenum and asked if we could use their space. The room with tables, sockets and internet is ideal for such meetings. The feedback was very positive and it was quickly agreed upon who could open up for us. An event on their website was also created that same evening. There was also a person from the hackerspace to join in!
​

**Fridtjof**:
This new person was me! Since the project was new to me, I mainly familiarized myself with the code first. A big compliment to the setup of the development environment - it worked smoothly and in no time I had my own foodsharing instance to tinker with. Since the hackathon, I've also finished a few MRs :)
​

**ctwx**: Arrived in hackerspace, I saw a few old faces and several new ones. We got right down to business and everyone took out their laptops and started hacking.
We hacked alone, in pair programming and everyone helped each other. I was amazed how much got done that day. The meeting was a great success. :) 
​

**christian-w** (Slack) und **chriswalg** (gitlab) (aus der Region Ludwigsburg / Stuttgart): I left work a bit earlier on Friday noon and took the train to Duisburg. There I was able to spend the night with Lea in the attic, which is completely open.
Lea and I talked about foodsharing and its programming even longer in the evening. On Saturday we took the train to Bochum to the Hackspace around 1 pm, because the event was supposed to start at 2 pm. Kevin, who recently joined the project and thankfully fixed <https://gitlab.com/foodsharing-dev/foodsharing/-/issues/705>, came without a laptop.
After a short time I asked him if he could have a look at something. It was about the Javascript Date Library date-fns, which we wanted to update to the current version 2.9.0, because we had worked with a version < 2.0 until then. Peter had already done a lot of preliminary work, I kept the branch up to date (git rebase master) and made some adjustments for the new registration form. But there was still a bug, which Kevin found after a long search. After that Kevin fixed another bug in the creation of business cards, so that the name of the district or the address, if they are too long, are wrapped and fit on the business card. In between, Kevin kept apologizing for using my laptop. Both improvements were then merged to the beta page the next day. 