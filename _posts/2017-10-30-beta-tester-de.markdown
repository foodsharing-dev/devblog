---
layout: post
title:  Was kann ich als foodsharing.de Beta-Tester tun?
ref:    how-beta-testing-works
date:   2017-10-30 17:15:00 +01:00
lang:   de
orig-lang: de
author: matthias
---

Schön, dass du die foodsharing.de Entwicklung als Beta-Tester unterstützen magst!
Damit du das sinnvoll tun kannst, lass mich dir kurz davon erzählen, wie die Entwicklung arbeitet:

Es gibt ein kleines Team von Entwicklern, welche in ihrer Freizeit mal mehr und mal weniger an der Entwicklung teilhaben.
Die Entwickler versuchen einerseits, Fehler zu beheben und sind andererseits dabei, den Code so umzustrukturieren, damit er leichter les- und bearbeitbar ist.
Zudem haben sie manchmal auch eine gute Idee, wie ihre eigene Arbeit (oder die von anderen) auf der Plattform vereinfacht werden kann.
Diese setzen sie dann manchmal um, oder auch nicht (Dies nennt sich "Feature").
Dann gibt es die Kommunikation mit Nutzern der Plattform oder administrativen Stellen wie z.B. dem Vorstand:
Von all diesen Stellen kommen viele viele Anfragen, die aufgrund gemachter Annahmen oder enthaltener Informationen in der Regel nicht attraktiv für Entwickler zu bearbeiten sind.
Deshalb werden sie meistens einfach ignoriert.
Wenn pro Woche in der Summe 30 Stunden in die Entwicklung von foodsharing.de investiert werden (1-20 Stunden pro Person), wird davon einiges für interne Kommunikation benötigt.
Die reine Entwicklungszeit beträgt nurnoch wenige Stunden pro Woche.
Soll davon nun auch noch etwas zur individuellen Kommunikation nach außen benutzt werden, so würden wir beim Stillstand ankommen.

<!--more-->

Und nun kommst du, lieber Beta-Tester:
Du hast wenigstens einen kleinen Einblick in das, was wir machen und kannst daher mit uns auf einer Art und Weise kommunizieren, dass wir uns gegenseitig Verstehen und effizient miteinander umgehen können.
Du hast bereits etwas Erfahrung mit der foodsharing Webseite und benutzt diese regelmäßig, sodass dir manchmal auch Details auffallen, die doch ungünstig gelöst sind.

Das Entwicklungsteam besteht - gemessen an der Aktivität - etwa zu 50% aus Menschen, welche nur des Englischen mächtig sind.
Deswegen beschränken wir uns intern in der Kommunikation auf Englisch und wünschen uns das auch an der Schnittstelle.

Als Beta-Tester hast du hauptsächlich eine Aufgabe:
Benutze [beta.foodsharing.de](https://beta.foodsharing.de) und schau, ob dir etwas auffällt.
Eines deiner Werkzeuge ist der [Changelog](https://beta.foodsharing.de/?page=content&sub=changelog), welcher im Fragezeichen oben verlinkt ist.
Alles, was du unter "Unreleased" liest sind Änderungen, welche auf der Beta-Seite bereits verfügbar sind, nicht jedoch auf der normalen.
Dies ist dein Anwendungsfeld.
Betrifft dich eine der Änderungen?
Hat eine der Änderungen negative Auswirkungen auf dein Arbeiten mit der Plattform?
Funktioniert sie nicht vernünftig?

Hier kommt dein Einsatz!

Bitte melde dich auf [Slack](https://slackin.yunity.org) an und geh in den #foodsharing-beta Kanal.
Hier kannst du mit anderen Beta-Testern kommunizieren und bekommst auch direkten Draht zu den Entwicklern.
Bitte melde dich zudem auf [Gitlab](https://gitlab.com/foodsharing-dev) an.
Im Projekt [Foodsharing issues](https://gitlab.com/foodsharing-dev/foodsharing/issues) kannst du mit in Form von Issues mit den Templates für Bugs oder Feature Requests Informationen in für Entwickler verständlicher Form festhalten.

Scheue nicht vor diesen "technischen Plattformen", die Benutzung ist wirklich ganz einfach.
Beschränke dich lediglich auf die Funktionen, welche für dich relevant sind.
Slack ist nichts weiter als ein Chat mit mehreren Kanälen.
Gitlab dient zur Source-Code Verwaltung und eben dem Aufgaben/Issue-Management.
Nur dieser Teil ist für dich relevant.

