---
layout: post
title:  New issue tracker
ref:    new-issue-tracker
date:   2018-03-18 22:50:00 +01:00
lang:   de
orig-lang: en
author: matthias
---

# Issue tracker nun mit im foodsharing Projekt

[Peter](https://gitlab.com/peter.toennies) hat festgestellt, dass Gitlab es auch unterstützt, einen öffentlichen Issue tracker mit geschlossenem Repository zu verbinden. Also haben wir das [foodsharing Projekt](https://gitlab.com/foodsharing-dev/foodsharing) jetzt öffentlich gemacht und lediglich den Zugriff auf den Sourcecode eingeschränkt.

Bitte nutzt jetzt den [Issue tracker](https://gitlab.com/foodsharing-dev/foodsharing/issues) im foodsharing Repository.
