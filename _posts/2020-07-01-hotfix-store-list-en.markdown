---
layout: post
title:  Was in der letzten Zeit geschah... Hotfix, neue Teamliste, Internationalisierung
ref:    hotfix-store-list
date:   2020-07-01 12:00:00 +02:00
lang:   en
orig-lang: de
author: caluera
translator: caluera
---


**Hotfix**

There was a hotfix in June which solved multiple problems. Some of these were introduced in the last release or were longer lasting, pressuring issues. 

We are sending quite a few emails compared to our sending frequency, thus it takes some time until they arrive. This does not only affect emails for notifications but also for registration and password changes. There were many requests for the support asking why the expected email wouldn't arrive. 
The priority of these emails was changed, such that they will be sent before other kinds of emails.

If an email can't be delivered, the corresponted adress gets added to a bounce list. We do not sent emails to these adresses. They now get a warning on their profile for being on the list with an explanation how to remove their adress from the list. 
There is also a more explicit explanation about the problem from the support in [this article](https://foodsharing.freshdesk.com/support/solutions/articles/77000299947-e-mail-sperre-im-profil) in German.

There was a bug which saved a false gender when registrating. This is fixed now. 

**New Team List**

There is a new team list in progress for the stores. It is implementend in Vue.js. Thereby is list is now much more mobile friendly. If you clicked on a person in the list before on a smartphone, their number was dialed. If you wanted to use any other option like viewing their profile, you had to go back to the browser. 

Furthermore are now those responsible for the store always on top of the list, even if they have a sleeping hat on. Thus they are found much faster now.

Moreover it is not marked anymore if a person was or is an ambassador for a region anymore. Before the same color was used for both. Such labels can also reinforce hierarchies, even though they should not be important in that context.

**Internationalization**

We have the first translations to English!

Before, there was a DIY system to embed texts in the foodsharing website. Now we follow standards, such that it is easier for new developers to start and it will be less effort to implement a language switch.

This means in more technical details that our project was adapted to the Symfony structure further. This includes a session variable in which the current language is saved and the file naming convention from Symfony. Thus we do not need our own translation helper and validation anymore, but can use Symfony directly. 

The old translations are changed ongoingly.