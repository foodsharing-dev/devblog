---
layout: post
title:  Introducing the foodsharing dev blog
ref: introducing
date: 2016-10-10 19:21:00 +0200
lang: en
orig-lang: en
author: nick
---

A lot has been happening with foodsharing development lately and we'd love to
share it more widely and publicly now.

We write from our developer perspective, so it will get technical at times, but should
hopefully be interesting for anyone involved with foodsharing.

<!--more-->

First a little backstory.

Foodsharing was originally written almost exclusively by the lovely
Raphael Wintrich. He put the best part of two years of hard work and dedication
into building the site. Most people would run out of motivation before completing it.

It has since been powering the foodsharing movement (around 150 thousand people)
to save huge amounts of food.

<img src="/images/foodsharing-740.jpg">

However active development lapsed a couple of years ago and has had only maintainance
fixes since then. Matthias Larisch has taken care of keeping the site running,
and him and Michael Knoth have been contributing code fixes.

Via the <a href="https://yunity.org">yunity</a> project I became interested in helping out. Foodsharing is not a site about sharing cat pictures or writing witty comments, the users are _really_ active - they may leave their house multiple times a week to cycle across the city and collect food destinated to be thrown away. This site deserves more developer love.

There are probably many theories why development has slowed, the only one that makes
sense to me is that _this stuff is hard_, over time I hope to learn exactly why.
For now the task is breathe life into the development again.

I'll write more posts to explain the steps we take and how it goes :)
