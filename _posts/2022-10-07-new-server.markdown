---
layout: post
title:  Neuer Server für foodsharing
ref:    neuer-server-2022
date:   2022-10-07 14:00:00 +01:00
lang:   de
orig-lang: de
author: stcz
---
## Serverumzug die Zweite
Über 3 Jahre ist es nun her, dass wir das letzte mal auf einen neuen Server (damals der Server dragonfruit) umgezogen sind. Seit dem ist foodsharing immer weiter gewachsen und mehr und mehr Menschen nutzen die Plattform. Das sorgt auch für einen erhöhten Bedarf an Ressourcen. 

Also haben wir wieder Kontakt zu unserem Server-Sponsor [Manitu](https://www.manitu.de/) aufgenommen. Die Antwort auf unsere Anfrage hätte nicht besser sein können – Manitu hat einen gebrauchten Server für uns, der unsere Anforderungen erfüllt und stellt ihn uns kostenfrei zur Verfügung. Lediglich um die Beschaffung von neuen NVMe SSDs haben wir uns selbst gekümmern. Kostenpunkt: 222€ finanziert aus Spenden. Wer dazu beitragen will kann gerne auch Spenden: [spenden.foodsharing.de](https://spenden.foodsharing.de/).

Nachdem wir in einigen E-Mails alle Details geklärt hatten, hat Manitu die Server-Hardware für uns hergerichtet. An dieser Stelle noch mal ein großes Dankeschön an das Team von Manitu für den hervorragenden Support.

Am 1.10. haben wir dann nach einiger Vorbereitung den Umzug der Plattform vollzogen. Im Folgenden die ganze Geschichte noch mal etwas ausführlicher.

## Planung

Bereits im Vorfeld haben wir überlegt, was wir alles ändern wollen mit dem neuen Server. Beim Betriebssystem sind wir bei [debian](https://www.debian.org/) geblieben, allerdings nun mit der aktuellsten Version bullseye. Die wohl größte Änderung ist, dass wir als Dateisystem nun [zfs](https://openzfs.github.io/) nutzen. zfs wurde insbesondere für Server entwickelt und bietet deutlich mehr Funktionen als klassische Dateisysteme. Durch den Wechsel mussten noch einige Änderungen z.B. an der Datenbank Konfiguration vorgenommen werden.
Des weiteren haben wir für die Zertifikate von certbot auf [acme.sh](https://github.com/acmesh-official/acme.sh) gewechselt und haben die Berechtigung der einzelnen Anwendungen weiter eingeschränkt, um den Schaden im Falle einer Sicherheitslücke weiter zu beschränken.

## Der neue Server ist da

Nachdem [manitu](https://www.manitu.de/) uns den Server vorbereitet hat, war natürlich die Frage nach dem Namen. Eine Umfrage ergab "onion". Nun ging es an die Installaton. Dafür haben wir ein Live Debian genutzt und das eigentliche OS mit [debootstrap](https://wiki.debian.org/Debootstrap) installiert. Bei der Einrichtung des zfs Dateisystems sind wir überwiegend der [Anleitung von OpenZFS](https://openzfs.github.io/openzfs-docs/Getting%20Started/Debian/Debian%20Bullseye%20Root%20on%20ZFS.html) gefolgt.
Das neue System war also schnell da. Dadurch, dass wir schon sehr lange unsere Infrastruktur mit [ansible](https://docs.ansible.com/ansible/latest/index.html) verwalten war es auch sehr einfach eine foodsharing Instanz und alle weiteren Programme die benötigt werden zum laufen zu bekommen. Das Problem ist nur, wie kommen die Daten vom alten Server auf den neuen.

# Die Umzugsvorbereitungen

Beim letzten Umzug wurde das kopieren der Daten und Datenbank noch händisch erledigt. Diesmal wollten wir es automatisieren. Also haben wir ein ansible playbook gebaut, dass die beiden Server auf den Umzug vorbereitet, den Code, die Zertifikate, die Daten und die Datenbank kopiert. Es funktioniert auch auf anhieb, also fast fertig.

*Problem*: Die Datenbank und die Daten müssen in einem konsistenten Zustand kopiert werden. D.h. sie können nur Kopiert werden, wenn die Seite aktuell nicht benutzt wird. Soweit so gut – bei unserer Datenbank die ~60GB groß ist dauert das knapp 2 Stunden. Natürlich schade, wenn die Seite so lange nicht erreichbar ist, deshalb haben wir noch ein mal nach Optimierungsmöglichkeiten gesucht. Unser Datenbankadmin hat noch mal etwas aufgeräumt und z.B. Alte Quiz Sessions gelöscht. Wir Admins haben noch etwas an der Datenbankkonfiguration geschraubt um die Datenbank möglichst schnell kopieren zu können. Mit folgendem Befehl konnten wir die Datenbank letzendlich am schnellsten auf den neuen Server übertragen. Ein Hoch auf Unix Pipes, die es ermöglichen in einem Befehl die Daten direkt in die neue Datenbank einpflegen und auf dem Dateisystem zur Sicherheit ablegen:
```
ssh root@dragonfruit.foodsharing.network "mysqldump --add-drop-table --add-locks --skip-comments --single-transaction --quick fsprod" | tee fsprod_migration.sql | mysql fsprod
```
Das Migrationsscript war also vorbereitet, mehrfach getestet, fehlt noch ein Termin für den Umzug. Die Auslastung des Servers zeigt, dass - abgesehen von Mitten in der Nacht – Samstag und Sonntag Vormittag die Nutzung der Seite am geringsten ist. Also haben wir uns für den morgen des 1.10.22 für den Umzug entschieden. 

# Der Umzug
Samstag morgen der 1.10. Ich war vor meinem Wecker wach. Also pünktlich um 07:46:23 das Migrationsscript angeworfen. Alles wird eingerichtet und installiert. 

08:03:06 kommt die Meldung: „We are now going to migrate.“
Kurz noch mal alles Prüfen und los gehts – foodsharing ist erstmal nicht erreichbar und die Datenbank wird umgezogen. Die Zwischenzeit habe ich genutzt um die DNS-Records anzupassen (die Kollegen in Österreich und der Schweiz haben das auch gemacht), und die Anpassungen an unserer CI/CD-Pipeline vorzunehmen.
Nach knapp 32 Minuten war die Datenbank umgezogen und um 8:40 war alles auf dem neuen Server vorbereitet. Was noch Fehlte: der neue Code aus der CI/CD-Pipline. Um 10:52:57 waren dann alles abgeschlossen und foodsharing war wieder erreichbar. 
An Feierabend war aber so früh noch nicht zu denken. Ein paar Dinge mussten noch angepasst werden – so haben wir z.B. neue Zertifikate für die verschiedenen Seiten ausgerollt, das Backup musste angepasst werden und noch weitere Kleinigkeiten. 

# Die erwarteten unerwarteten Probleme
Selten läuft alles Reibungslos. Bereits Ende März hatten wir durch ein Update von MariaDB Probleme mit dem Chatserver. Bei unseren Tests auf dem neuen Server trat das Problem nicht auf – jetzt wo wir live waren plötzlich schon.
Wir hatten also ein akutes Problem. Wir hätten zurück auf den alten Server gekonnt, aber eigentlich musste das Problem irgendwann gefunden und gelöst werden.
Also schnell unsere Entwickler zur Hilfe gerufen.
Die Stelle mit dem Problem war bereits bekannt. Die Funktion [fetchAllByCriteria is](https://gitlab.com/foodsharing-dev/foodsharing/-/blob/master/src/Modules/Core/Database.php#L80-L83) gibt ein leeres Array zurück, wenn die Anfrage zu groß ist. Warum? 🤯 

Nach ein paar Tests konnte das Prolem schnell weiter eingegrenzt werden. Aber eine Lösung musste her. Durch das manuelle erstellen der Query, wie die Daten Abfragt konnte das Problem behoben werden.

# Abschluss
Trotz der Probleme sind wir froh auf den neuen Server umgezogen zu sein. Die Auslastung ist nicht mehr durchgängig am Anschlag und die Ladezeit der Seite hat sich wieder verbessert. Wer sich das anschauen möchte kann das [hier](https://onion.foodsharing.network/foodsharing.network/onion.foodsharing.network/index.html) tun. Außerdem haben wir auch erstmal wieder Platz die hochgeladenen Daten und die wachsende Datenbank zu speichern. An dieser stelle noch ein mal Danke an [manitu](https://www.manitu.de/), die es uns möglich machen eine große Seite wie foodsharing kostenlos zu betreiben. Den bisherigen Server wollen wir in Zukunft für die foodsharing Cloud und andere Dienste nutzen.  
