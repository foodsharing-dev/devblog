---
layout: post
title:  Foodsharing-Development Infomail
ref:    foodsharing-dev-infomail
date:   2017-08-23 16:48:00 +02:00
lang:   de
orig-lang: de
author: tilmann
---

## Wie sieht es mit der Weiterentwicklung von foodsharing.de aus? Wer ist denn aktuell daran beteiligt?

Zunächst einmal als Übersicht eine Liste von Menschen, die aktuell an der Foodsharing.de-Entwicklung beteiligt sind.

<!--more-->

- [Raphael](https://foodsharing.de/profile/56) arbeitet im Rahmen des [Protoype Funds](https://prototypefund.de/project/foodsharing/) an foodsharing.de und möchte den Code einerseits umstrukturieren und ihn andererseits unter eine Open-Source-Lizenz stellen, damit noch mehr Leute daran mitarbeiten können. Er hat damals die ursprüngliche Plattform lebensmittelretten.de im Alleingang programmiert und kennt den alten Code am besten. Nach dieser Mammutaufgabe hat er sich erstmal komplett aus dem Instandhaltungs-Tagesgeschäft herausgezogen. Jetzt ist er wieder da, wird aber auch gerade Vater und hat dementsprechend begrenzte zeitliche Ressourcen.
- [Matthias](https://foodsharing.de/profile/7995) ist hauptsächlich an foodsharing.de/foodsharing-light tätig. Hauptsächlich heißt jedoch trotzdem, dass zur Zeit im Durchschnitt weniger als 1-2 Stunden pro Woche dort investiert werden. Er kennt den foodsharing.de Code, hat ihn einige Monate alleine gewartet. Er ist zusammen mit Raphi der einzige mit Zugang zum Server und mit Kristijan zur Datenbank - sie sind die einzigen, die auch noch Dinge tun können, welche man nicht in der Plattform klicken kann. Zudem werden an ihn viele Anfragen weitergeleitet.
- [Nick](https://foodsharing.de/profile/98514) arbeitet an foodsaving.world mit und hat die neue foodsharing.de Entwicklungsinfrastruktur aufgebaut (Gitlab, docker-compose setup, foodsharing light)
- [Tilmann](https://foodsharing.de/profile/93896) ist hauptsächlich an foodsaving.world tätig und übernimmt auch andere Kommunikationsaufgaben zum Thema foodsaving worldwide. Ein bisschen ist er auch an der Entwicklung von foodsharing light beteiligt.
- [Janina](https://foodsharing.de/profile/37093) arbeitet an foodsaving.world mit, koordiniert die Internationalisierung von foodsharing und war auf dem Delegiertentreffen in einer foodsharing.de IT Besprechungsgruppe.
- [Sam](https://foodsharing.de/profile/141766) will die Kommunikation zwischen der foodsharing.de-Community und den EntwicklerInnen verbessern.
- [Lars](https://foodsharing.de/profile/125409) arbeitet an foodsaving.world mit und neuerdings auch an dringenden foodsharing.de-Änderungen
- [Peter](https://foodsharing.de/profile/116481) sucht in der Schweiz nach EntwicklerInnen für foodsharing.de und hilft auch selbst mit.


## Wohin können sich IT-Interessierte wenden?

Am Besten wäre es, wenn alle Interessierten direkt [dem yunity-Slack beitreten](https://slackin.yunity.org) und dort in den #foodsharing-dev Channel schreiben. Die Kommunikation aller oben gelisteter Menschen läuft hauptsächlich über diesen yunity Teamchat. Die Hauptsprache dort ist zwar Englisch, aber auch Posts auf Deutsch sind völlig ok. Um aber die Einstiegshürde zu nehmen und eine Weiterleitungsmöglichkeit zu geben, haben wir jetzt eine Sammeladresse eingerichtet: [foodsharing-dev@yunity.org](mailto:foodsharing-dev@yunity.org)
E-Mails an diese Adresse werden in den Slack-Channel weitergeleitet, sodass sie dort für alle sichtbar ist und auch alle antworten könnten. Es ist wichtig, dass diese Adresse nicht für Support-Anfragen etc. genutzt wird, nur für Menschen, die uns helfen möchten und deshalb direkten Kontakt zu uns benötigen.

## Eine kurze Übersicht über die Projekten und die aktuellen Aufgaben & Herausforderungen

### [Foodsharing.de](https://foodsharing.de) Entwicklung

Ziel: Bugfixes und dringende Änderungen möglich machen

Herausforderungen:

- Besseren Prozess entwickeln, um eingehende Issues zu sortieren (triage) und die EntwicklerInnen nicht zu "erschlagen". Dazu gibt es eine [Issue im Gitlab](https://gitlab.com/foodsharing-dev/issues0/issues/224) - wir erbitten euren Input hierzu!
- Automatisches Beta-Deployment einrichten, um Änderungen einfacher zu testen
- Nick den Zugang zum foodsharing.de-Server ermöglichen, um die Production-Version unabhängig von Matthias zu deployen
- Mehr code review von unseren EntwicklerInnen einfordern
- Test-Abdeckung erhöhen, um EntwicklerInnen mehr Sicherheit bei Änderungen zu geben
- Beteiligte Personen offensichtlicher machen -> evtl eine Foodsharing-Dev Teamseite erstellen?

### [Foodsharing light](https://beta.light.foodsharing.de)

Ziel: den BenutzerInnen von foodsharing.de möglichst schnell eine moderne mobile Webseite zur Verfügung zu stellen

Herausforderungen:

- Features identifizieren, welche a) einfach umsetzbar und b) dringend benötigt werden
- Wir stoßen bereits jetzt auf Situationen, wo eine noch stärkere Kopplung mit foodsharing.de erforderlich ist. Hier Komponenten richtig zu trennen bzw. ganz auf das neue Foodsharing light zu überführen, erfordert Anpassungen an der bestehenden Webseite und schafft eventuell neue Abhängigkeiten in die andere Richtung (Beispiel: Chat-System, Versand von Emailbenachrichtigungen)

### [Foodsaving.world](https://foodsaving.world)

Ziel: Webseite & App für LebensmittelsretterInnen-Gruppen auf der ganzen Welt zur Verfügung zu stellen

Herausforderungen:

- (keine großen, es geht stetig voran)
- mehr DesignerInnen und Frontend-EntwicklerInnen finden

Anmerkung: foodsaving.world unterstützt nicht die aktuellen Organisationsstrukturen von foodsharing.de und eine Integration derer ist nicht 1:1 geplant (aufgrund anderer Philosophie, zu weniger EntwicklerInnen). Mehr dazu in diesem [Blog-Post](https://blog.foodsaving.world/2017/08/13/ein-paar-antworten.html)

_Geschrieben von Tilmann und Matthias, in Absprache mit Nick und Janina_
