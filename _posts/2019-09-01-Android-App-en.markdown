---
layout: post
title:  Intermediate status of the Android App
ref:    android-app
date:   2019-09-01 12:00:00 +02:00
lang:   en
orig-lang: de
author: jo
translator: jonathan
---


# **project objective**

We had considered that we needed an app to improve the manageability of food baskets for all users of www.foodsharing.de
In the long run, all foodsaver functions will be built on this basis without replacing the website.
Advantages of a smartphone app are the "sensors" that come with it, such as camera, GPS and positioning.
We have yet to focus on the development of the Apple (iOS) App.  

![](/images/02_conversations.png)
_This is what the chat currently looks like_

<!--more-->

# **project progress**
The project started a year ago with strong support from Nick Sellen. 
It did not really take off until October 2018, when another programmer joined. The mutual motivation bore fruit. The chat function developed into the well-functioning core of the app.
At the Hackweek in February Nick gave the app into the hands of the team, consisting of Alex, David, Janosch, Lukas, Diego, Simon, WUUUGI, and I. We continued the already planned functions and published a ["Public Beta" (public test version) in the "Google Play Store"](https://play.google.com/store/apps/details?id=de.foodsharing.app) on 20.04.2019.

On 16.08.2019, the [foodsharing festival](https://www.foodsharing-festival.org/), the download counter reached the 10,000th download (to different device accounts). 
A small but very active team is now working on improvements every day: From the day the project started until today 516 commits have been made. 

The first public Beta **Version 0.1.0 released in April 2019** has been followed by four more versions to date, so that we arrived at **Version 0.4.0**** in August. 
Until now have an update cycle of about 45 days, but are trying to shorten the cycles.

Currently there are approximately. **85 installations per day**, so the number of users is growing rapidly. We have refrained from explicit advertising within the community so far in order to keep support requests few. (If you want to help us as a supporter, you'll find our contact data below.)
Currently a new version of an internal "foodsharing beta" app is released internally every 3 days. After a request via the mentioned links there are still about 80 "test places" available. In cooperation with a slack chat group, so-called "test calls" are followed here to inform the testers about detailed changes and to work hand in hand.

# **call for assistance**

Maybe you, dear reader, are an enthusiastic user of the app or even a programmer; a person capable of learning with some time and the will to get involved. Maybe you even want to develop the Apple (iOS) App?
Then take a look at our [call for cooperation](https://devdocs.foodsharing.network/it-tasks.html) and get in touch with us (for example by e-mail: it@foodsharing.network). 
We look forward to hearing from you.