---
layout: post
title:  foodsharing is finally Open Source!
ref:    open-source
date:   2019-02-23 17:19:00 +02:00
lang:   de
orig-lang: de
author: janina
---

# foodsharing ist endlich Open Source!

## Aber was bedeutet das eigentlich?

Das bedeutet, dass unser grundlegendes Prinzip vom Retten und Teilen jetzt auch auf die Plattform selbst angewandt ist. JedeR kann jetzt im Repository auf GitLab den [Code von foodsharing](https://gitlab.com/foodsharing-dev/foodsharing).  einsehen, runterladen und verändern. Aber keine Sorge, dabei geht es immer nur um eine Kopie unserer Seite und wenn jemand bei sich zuhause am Rechner am foodsharing-Code rumspielt, dann hat das natürlich keinerlei direkte Auswirkungen auf das, was auf foodsharing.de passiert. Auf der anderen Seite ist es allerdings auch nicht schwierig, tatsächlich Änderungen an unserer Homepage vorzunehmen. Dafür müssen die zuhause gemachten Änderungen einfach nur zur Quelle zurückgeschickt und gemeinsam mit unserem Entwicklerteam eingepflegt werden. Vielleicht beginnt man mal mit der Korrektur eines Rechtschreibfehlers; vielleicht kommt aber auch mal eine neue Community und beginnt mit der kompletten Übersetzung in eine andere Sprache. Der Code ist jetzt für alle offen und Änderungsvorschläge können jederzeit hochgeladen werden.

Vor allem aber sind wir jetzt sicher, dass unser schönes Projekt nicht hinter verschlossenen Türen versauert, sondern dass es frei geteilt werden kann - und das ist es doch, worum es bei foodsharing geht.

Das ist aber noch nicht alles: Als Open Source Projekt haben wir neue Möglichkeiten gefördert zu werden, denn alles was wir jetzt an Programmierarbeit tun ist offensichtlich gemeinnützig. Als erstes wird sich das für die EntwicklerInnen selbst auszahlen, da wir Open Source Lizenzen für hilfreiche Programme, die die Entwicklungsarbeit einfacher machen, beantragen werden. Längerfristig können wir uns aber auch um Fördergelder bewerben, die für Open Source Projekte zur Verfügung stehen.

Die Hackweek in Wurzen ist noch nicht komplett vorbei, und die Laune ist nach wie vor bestens! Vorgestern Abend gab es eine kleine Feier um das Open Sourcing gebührend zu zelebrieren, und für detailliertere Infos zu dem, was sonst so im Entwicklerteam los ist, schaut in [diesen ausführlichen Changelog-Entry](https://gitlab.com/foodsharing-dev/foodsharing/blob/master/CHANGELOG.md#2019-02-21).



## Lust, dabei zu sein?

Hat dich dieser Beitrag angefixt, auch Teil eines ehrenamtlich arbeitenden Teams mit diversesten Aufgaben und dem Interesse, einfach mal neue Dinge ausprobieren zu können, zu werden?  
[Hier](https://devdocs.foodsharing.network/it-tasks.html) findest du unsere Devdocs, mit einem Teil der Aufgaben, bei denen wir noch nach Unterstützung suchen.  