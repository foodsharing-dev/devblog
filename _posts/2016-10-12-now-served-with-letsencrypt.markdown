---
layout: post
title:  Devblog now served via https/letsencrypt
ref:    hello-letsencrypt
date:   2016-10-12 03:06:00 +02:00
lang:   en
orig-lang: en
author: matthias
---

As we are still in the process of getting our tools and environment set up, this
is a nice opportunity for me to as well look into gitlab CI and this blog system.

If you may not have noticed, this blog is maintained in a
<a href="https://gitlab.com/foodsharing-dev/foodsharing-dev.gitlab.io">gitlab repository</a>
, runs on the <a href="https://github.com/jekyll/jekyll">Jekyll static site generator</a>
and is written in markdown.

<!--more-->

While it is really nice from gitlab to automatically serve the compiled version through
their pages system (also on our own domain, if we would like to), setting up https
is a hassle, when you want to use letsencrypt.org certificates.

So, as we have the infrastructure for hosting different kinds of mini projects
already available on our main yunity project server, its codename is yuca,
why not host it there?

That server is setup to serve all pages only ever through https. We do that
through a simple force-https virtual host, that keeps support for serving the
.well-known path used for letsencrypt domain verification directly over http:

{% highlight nginx %}
server {
  listen 80 default_server;
  listen [::]:80 default_server;
  server_name _;

  root /var/www/html;

  location /.well-known/ {
    allow all;
    try_files $uri $uri/;
  }

  location / {
    return 301 https://$host$request_uri;
  }
}
{% endhighlight %}

For each site, we expect a folder for it to exist that contains a subfolder `cert`
with a file called `dns` that just contains the domain name which should be
used in the certificate.

Our certificates are created by a monthly cronjob executing the following script:

{% highlight bash %}
#!/bin/sh
DEFAULT_ROOT=/var/www/html
BASE_ROOT=/var/www/
SIMP_LE=/root/simp_le/venv/bin/simp_le
EMAIL=mail@yunity.org
SITES=`find $BASE_ROOT -type d -name cert`
SERVICE=/usr/sbin/service
for I in $SITES; do
cd $I
DOMAINS=""
if [ -f dns ]; then
for DNS in `cat dns`; do
    if [ -n "$DNS" ]; then
    DOMAINS="-d $DNS $DOMAINS"
    fi
done
fi
echo "in folder" `pwd`
$SIMP_LE --email $EMAIL -f account_key.json -f fullchain.pem -f key.pem $DOMAINS --default_root $DEFAULT_ROOT --tos_sha256 6373439b9f29d67a5cd4d18cbc7f264809342dbf21cb2ba2fc7588df987a6221 && $SERVICE nginx reload
done
{% endhighlight %}

With all that already in place, it should just be 5 minutes to deploy our blog there.
Reality went into 2-3 hours with some parallel social interaction...
See [our CI script](https://gitlab.com/foodsharing-dev/foodsharing-dev.gitlab.io/blob/master/.gitlab-ci.yml)
for details. It just needs you to setup a SSH private key in your project secret
variables so gitlab can directly rsync to your server. Beware to use a rsa key
without a comment (`-t rsa -C ""`), otherwise it did always ask for a passphrase
and fail the deploy here. Also, I did not manage to get host key verification working.
Not really an issue in this scenario, though.
