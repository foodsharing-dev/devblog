---
layout: post
title:  Hackweek Frühling 2020 Rückblick
ref:    hackweek-spring-2020-review
date:   2020-05-22 13:00:00 +01:00
lang:   de
orig-lang: de
author: felix, amazing_k, caluera, moffer, chris
---



# Blogpost über die Hackweek Mai 2020 - ein Rückblick

[Felix] Vor der Hackweek war ich unsicher, ob ich mich beteilige und habe mich daher leider nicht an der Vorbereitung beteiligt. Diese Vorbereitung lief erst rech schleppend an, aber in der Zeit kurz vor der Hackweek füllte sich das Programm mit Themen und initiierten Sessions. Für mich als Immer-noch-Neuling haben diese die Möglichkeit gegeben, zuzuhören, Fragen zu stellen und damit zu lernen.

[Kristijan]
Die Hackweek ist für mich ein freudiges Ereignis um viele der aktiven Namen auch mal live zu erleben. Corona hat dies leider erschwert. Umso erfreulicher war es, das es eine "online Hackweek" gab. Sessions wurden in Videokonferenzen abgehalten, so konnte sich zumindest mal gesehen werden und haben wesentlich zu einem Gruppengefühl und einem "wir" beigetragen. Es entstand eine schön produktive Atmosphäre und es war wertvoll Feedback und neue Ideen für Umsetzungen zu erhalten. Es gab auch lustige Sessions zu eigentlich ernsten Themen, die damit aufgelockert wurden. 

[Caluera]
In der Hackweek haben wir richtig viel erledigt bekommen, gerade auch Sachen, die sonst im Alltag eher untergehen. Wir haben viel auch über Strukturen gesprochen und Planungen gemacht, wie wir mit manchen Themen umgehen werden. 

[moffer]
Die Hackweek hat einiges vorangebracht, für mich vor allem auch soziale Aspekte: Mit einigen Leuten quasi live reden, mit denen man sonst nur über Gitlab oder Slack zu tun hat. Auch fand ich die gesamte Stimmung positiv. Es wurde reichlich diskutiert, wodurch zum Hacken gar nicht so viel Zeit blieb - bei mir war leider durch private Gründe die Zeit auch recht begrenzt. Ich kann mir jedoch vorstellen, dass so ein Onlineformat auch öfters stattfinden kann, da sich der Orga-Aufwand (und evtl finanzieller Aufwand) in Grenzen hält. Ich würde das begrüßen.

[Chris]
Besonders begeistert hat mich das Gemeinschafts-Gefühl trotz der räumlichen Entfernung :) Insgesamt viele spannende Einblicke in ganz unterschiedliche Bereiche, produktive Diskussionen und ein am Nutzer orientierter Austausch, dazu eine flexible Organisation mit mehr Struktur genau da, wo es nötig war - ich würde mich über eine baldige Wiederholung definitiv freuen und hoffe auf viele auch neue Gesichter.  
Also, ihr Leser da draußen: Schaut einfach beim nächsten Mal unverbindlich rein, bei einigen Sessions vorbei und bringt euch vielleicht direkt ein. Hackweeks sind nämlich eine super Gelegenheit, das Team und die Arbeitsweise kennenzulernen - auch und insbesondere dann, wenn ihr nicht selbst mit programmiert!
