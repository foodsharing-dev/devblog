---
layout: post
title:  foodsharing is finally Open Source!
ref:    open-source
date:   2019-08-26 17:00:00 +02:00
lang:   en
orig-lang: de
author: janina
translator: jonathan
---

# foodsharing is finally Open Source!

## What does that actually mean? 

This means that our basic principle for food (of saving and sharing) is now applied to the platform itself. Anyone can now view, download and modify [the foodsharing code in the GitLab repository](https://gitlab.com/foodsharing-dev/foodsharing). 
But don't worry, it's always about a copy of our site and if someone plays around with the foodsharing code at home, it has no direct effect on what happens on foodsharing.de. 
On the other hand, it is not difficult to actually make changes to our homepage. To do this, the changes made at home simply have to be sent back to the source and updated together with our development team. Maybe you start by correcting a spelling mistake; maybe a new community comes along and starts with the complete translation into another language. The code is now open to everyone and change suggestions can be uploaded at any time.

Most importantly, we're now sure that our beautiful project won't go to waste behind closed doors, but can be freely shared - and that's what foodsharing is all about.

But that's not all: As an open source project, we have new opportunities to be supported, because all the programming we do now is obviously charitable. First of all this will pay off for the developers themselves, as we will apply for open source licenses for ‚helpful programs that make the development work easier‘. In the longer term, however, we can also apply for funding that is available for open source projects.

The Hackweek in Wurzen was great! There even was a small celebration to celebrate Open Sourcing.
For more detailed information about what's going on in the development team, have a look at [this detailed changelog entry](https://gitlab.com/foodsharing-dev/foodsharing/blob/master/CHANGELOG.md#2019-02-21).


## Wanna join and collaborate?

Are you thinking about joining the volunteer team in their work with diverse tasks?
Are you interested to try new things?  
[Here you can find our Devdocs](https://devdocs.foodsharing.network/it-tasks_EN.html) with some of the tasks we need support for.