---
layout: post
title:  Wiki, Mumble und Mediendatenbank auf neuem Server
ref: 2016-10-new-server
date: 2016-10-16 22:05:00 +0200
lang: en
orig-lang: de
translator: fenja
author: matthias
---

Good evening, dear blog readers!

For a few months now, we still had a server available on which Kristijan wanted to make some Wiki tests for the internationalization.
But it wasn't used in the end and therefore available for rescue.

<!--more-->

Also, it turned out that the setup of the server of our [media database](https://media.foodsharing.de) 
has been a bit too open which is why we unfortunately had to switch it off last week.

Therefore it seemed to be a good idea to move the media database anyhow. The Wiki as well as the Mumble server
were running, together with other projects, on a private VServer of Raphael,
thus today I got the ambition to clean up and move both of them, too.

So from today on the Wiki, Mumble and the media database are on a new server, internal we
named it carrot (since the server of the foodsharing.de site is called banana).

For you nothing changes. The URLs have also been changed consistently to foodsharing sub domains and 
HTTPS is activated. The old domains are still working of course so that your bookmarks are
forwarded automatically.


* [media database](https://media.foodsharing.de)
* [Wiki](https://wiki.foodsharing.de)
* Mumble: mumble.foodsharing.de

