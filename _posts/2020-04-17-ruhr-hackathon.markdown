---
layout: post
title:  Programmieren im Pott
ref:    ruhr-hackathon
date:   2020-04-17 04:10:00 +01:00
lang:   de
orig-lang: de
author: 
- caluera
- christian-w
- ctwx
- fridtjof
---



Am 15.02.2020 haben sich einige aus der foodsharing IT in Bochum getroffen, um an der foodsharing Webseite weiterzuarbeiten.
Hier sind Berichte von einem Teil der Anwesenden:

<!--more-->

**Caluera**: 
Um die Veranstaltung bekannt zu machen, habe ich einen NRW weiten Termin auf der foodsharing Webseite erstellt, damit vielleicht auch die Leute mitmachen, die bisher noch nicht an der foodsharing Programmierung mitgemacht haben. Darüber haben sich auch ein paar Interessierte gefunden, bei denen es meist leider zeitlich nicht geklappt hat. 
Außerdem habe ich mich an die Social Media AG gewandt, die einen Aufruf für unser Treffen auf Twitter veröffentlicht hat. 
Wir haben uns beim Hackerspace [das Labor](https://wiki.das-labor.org/w/LABOR_Wiki) in Bochum getroffen. Ich bin dafür zu deren Plenum gegangen und hab nachgefragt, ob wir deren Raum nutzen dürfen. Der Raum ist mit Tischen, Steckdosen und Internet ideal für solche Treffen. Die Resonanz war sehr positiv und es war auch schnell abgesprochen, wer für uns aufschließen kann. Eine Veranstaltung auf deren Webseite war auch am selben Abend noch erstellt. Es hat sich auch noch eine Person aus dem Labor zum Mitmachen gefunden!

**Fridtjof**:
Diese neue Person war dann wohl ich! Da das Projekt für mich neu war, habe ich mich hauptsächlich erstmal vertraut mit dem Code gemacht. Hierbei ein großes Lob an die Einrichtung der Development-Umgebung - die hat reibungslos geklappt und im Nu hatte ich meine eigene Foodsharing-Instanz zum basteln. Seit dem Hackathon sind meinerseits schließlich auch schon ein paar MRs fertig geworden :)

**ctwx**: Angekommen im Hackerspace gleich ein paar alte Gesichter sowie mehre neue gesehen. Wir sind gleich zur Sache gekommen und alle haben ihre Laptops rausgeholt und angefangen zu hacken.
Es wurde einzeln gehackt, im Pair Programming und alle haben sich gegenseitig geholfen. Ich war erstaunt, wie viel an dem Tag umgesetzt wurde. Das Treffen war ein großer Erfolg. :) 

**christian-w** (Slack) und **chriswalg** (gitlab) (aus der Region Ludwigsburg / Stuttgart): Habe am Freitag Mittag etwas früher die Arbeit verlassen und bin mit dem Zug nach Duisburg. Dort konnte ich dankenswerterweise bei Lea in der WG im Dachboden, der komplett offen ist, übernachten.
Lea und ich haben uns dann noch länger am Abend über foodsharing und deren Programmierung angeregt unterhalten. Am Samstag fuhren wir dann gemeinsam gegen 13 Uhr mit dem Zug nach Bochum zum Hackspace, da die Veranstaltung um 14 Uhr beginnen sollte. Kevin, der erst kürzlich zum Projekt dazu gestoßen ist und dankenswerterweise <https://gitlab.com/foodsharing-dev/foodsharing/-/issues/705> behoben hat, kam ohne Laptop. Habe ihn dann nach kurzer Zeit gefragt, ob er sich mal etwas anschauen kann. Es ging um die Javascript Date Library date-fns, die wir auf die aktuelle Version 2.9.0 aktualisieren wollten, da wir bis dato noch mit einer Version < 2.0 gearbeitet hatten. Peter hatte schon große Vorarbeit geleitet, ich habe den Branch auf dem aktuell Stand gehalten (git rebase master) und noch Anpassungen für das neue Registrierungsformular eingebaut. Es gab allerdings noch einen Fehler, den Kevin nach langem suchen gefunden hat. Danach hat Kevin noch einen Fehler in der Erstellung von Visitenkarten behoben, damit der Bezirksname oder die Adresse, wenn diese zu lang sind, umgebrochen werden und auf die Visitenkarte drauf passen. Zwischendurch hat sich Kevin immer wieder entschuldigt, dass er mein Laptop benutzt. Beide Verbesserungen wurde dann am nächsten Tag auf die beta-Seite gemerged. 