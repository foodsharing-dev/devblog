---
layout: post
title:  New Year Progress Review
ref:    new-year-progress-review
date:   2017-02-06 21:46:00 +02:00
lang:   en
orig-lang: en
author: nick
---

We have a new year now and seems a good time for a little review of progress.

Watching our attempt to kickstart the development of
[foodsharing.de](https://foodsharing.de)
has been fascinating for me. This stuff is tricky, and there is no guarantee of
success.

The starting premise was that the project is very worthwhile with a great
community of foodsavers and there are no viable alternative software platforms.
It's too good to abandon!

We achieved some key things:
- docker based development environment setup
- tests
- CI system to build/test on each push
- a clearer path for contributors to join ([slack channel](https://slackin.yunity.org/), [public gitlab group](https://gitlab.com/foodsharing-dev/))
- new contributors
- 45 closed issues
- 82 merged merge requests

Right now though, development has slowed again. This is OK, projects move in
many cycles of rise and fall. If we give up as soon as things get tricky we
cannot achieve much.

Lets dig into some of the difficulties...

<!--more-->

### Backed up changes

There are many changes that have become stuck in limbo. Code has been written,
comments made, then... well... nothing. Too many of these and motivation is
sucked dry.

A good example is
[this one](https://gitlab.com/foodsharing-dev/foodsharing/merge_requests/74).

As a contributor this is frustrating, why contribute code if it goes nowhere.

Another type is WIP (Work In Progress) contributions - the author has not
marked it ready, but maybe just stopped work.

An example is my own experiments at
[adding vue.js](https://gitlab.com/foodsharing-dev/foodsharing/merge_requests/100)
for frontend rendering. It's just a proof of concept so needs more discussion.
But without any input on the merge request it went stale (might pick it up again
though!).

### Introducing bugs during feature changes

Some new bugs were added to the live site after deploying changes.
This led to extra caution for further changes.

It's tricky for a reviewer to assess whether a given change will introduce new
bugs due to low test coverage and some structural difficulties in the code (not
so modular in places).

### Too much dependence on Matthias

Matthias has kept the platform alive and running more-or-less single handed.
Almost all development or operational changes must pass through Matthias right
now.

He has the best knowledge of the codebase, and is the only person that
can deploy (due to data protection related server access limitations).

Also, he has many other things to be doing besides foodsharing.de, he's an
extremely useful person to have on your team!

### Issues accumulate faster than we can resolve them

[126 open issues](https://gitlab.com/foodsharing-dev/foodsharing/issues) is too
many. Kind of overwhelming. Where to start?

### Backend is not moved to symfony yet

One of the goals we had was to move the backend to symfony to make it
more modular, easier to do unit testing, and give us access to the wide range
of [symfony components](http://symfony.com/components).

This should be a key change that unlocks other benefits, but has a high initial
cost.

### Frontend is not modernized

I started an [experiment to port the frontend to vue.js](https://gitlab.com/foodsharing-dev/foodsharing/merge_requests/100).
But then stopped.

If completed it will allow frontend testing, a nice modular structure, and maybe
interest frontend devs or designers.

### Still not open source

Not being open source is a barrier to entry for many developers and it makes
introducing new developers trickier (they have to find their way to our slack room and request access).

And why give up your time to a propriety project? Indeed, I contribute on the
 basis it will eventually become open source.

This has involved many discussions between people in the project, but progress
has been made! ... should be on the way.

### Not attractive enough to new developers

The hackathon was great and unleashed a fury of activity for a while. But we
have not managed to keep new contributors for the longer term.

### Lack of development resources

This is perhaps just a simple root cause of everything. It can't be overstated
how few resources this development project has: perhaps the equivalent of 0.05
full time developers.

There is always too much to do, and it's not always clear in which order things
should be done in.

## Solutions?

Whilst slow, progress has not halted. Given the lack of resources we need to
_work smart_ and make the most of what we do have.

The focus for me is on reducing friction for developers to work on features and
bug fixing. This splits into two parts: technical and human process.

The technical part should help people to write modular and tested code that can
be merged easily, some goals are:
- renewed effort to move to symfony components
- continue to work on open sourcing
- refactor the frontend to a modern framework

The human aspect is perhaps more significant though.

Everybody involved has many other things to do, so keeping energy/motivation
flowing is a challenge, especially being a remote team.

A few ideas:
- regular scheduled calls/updates
- a clearer and publicly stated plan
- agree work plans for people first - don't just start work
- regular issue/merge request management/triage/cleanup
- explicit responsibilities

On the last point, I was thinking a responsibilities audit might be useful; it
would reveal how many things leads to Matthias. But it also ties into some
 thoughts I've been having about explicit responsibilities, what if people were
 asked to agree to commit to:
- making sure all issues are labelled correctly?
- ensuring all open questions on merge requests get addressed?
- introducing new contributors to the project?
- reviewing 3 merge requests per month?

It's still a mystery to me though! Maybe just talking/writing about it is enough
to inspire some motivation...