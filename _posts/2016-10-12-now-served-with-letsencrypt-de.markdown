---
layout: post
title:  Devblog wird jetzt durch HTTPS/Let's Encrypt ausgeliefert
ref:    hello-letsencrypt
date:   2016-10-12 03:06:00 +02:00
orig-lang: en
lang:   de
translator: fenja
author: matthias
---

Da wir gerade noch dabei sind, unsere Werkzeuge und Umgebung einzurichten, ist 
das für mich eine gute Gelegenheit gitlab CI und dieses Blog-System auch
genauer anzuschauen. 

Falls ihr es noch nicht bemerkt habt: dieser Blog wird in einem 
<a href="https://gitlab.com/foodsharing-dev/foodsharing-dev.gitlab.io">gitlab repository</a>
verwaltet, mittels des <a href="https://github.com/jekyll/jekyll">Jekyll Generators</a>
erzeugt und ist in [Markdown](https://de.wikipedia.org/wiki/Markdown) geschrieben.

<!--more-->

Während es wirklich gut ist, dass Gitlab automatisch die compilierte Version mittels
Gitlab Pages ausliefern kann (auch mit unserer Domain, wenn wir möchten), 
ist es ein ziemlicher Aufwand HTTPS einzubauen, wenn man letsencrypt.org Zertifikate
benutzen will.

Also, da wir die Infrastruktur, um verschiedene Arten von Mini-Projekten zu hosten ,
schon auf unserem Haupt-yunity-Projekt-Server verfügbar haben - dessen Code-Name
ist yuca - warum hosten wir es nicht dort?

Dieser Server ist so eingerichtet, dass er alle Seiten immer nur per HTTPS
ausliefert. Wir machen das mithilfe eines einfachen virtuellen Hosts, `force-https`,
der den `.well-known` Pfad für die Let's Encrypt Domain-Verifizierung über HTTP ausliefern,
alles andere jedoch nach HTTPS weiterleitet:

```
server {
  listen 80 default_server;
  listen [::]:80 default_server;
  server_name _;

  root /var/www/html;

  location /.well-known/ {
    allow all;
    try_files $uri $uri/;
  }

  location / {
    return 301 https://$host$request_uri;
  }
}
```

Wir gehen davon aus, dass für jede Seite ein Ordner existiert, der einen 
Unterordner `cert` beinhaltet, mit einer Datei `dns`, die die Domain-Namen beinhaltet,
die im Zertifikat benutzt werden sollte.

Unsere Zertifikate werden durch einen monatlichen cronjob erstellt, der das 
folgende Skript ausführt:

```
#!/bin/sh
DEFAULT_ROOT=/var/www/html
BASE_ROOT=/var/www/
SIMP_LE=/root/simp_le/venv/bin/simp_le
EMAIL=mail@yunity.org
SITES=`find $BASE_ROOT -type d -name cert`
SERVICE=/usr/sbin/service
for I in $SITES; do
cd $I
DOMAINS=""
if [ -f dns ]; then
for DNS in `cat dns`; do
    if [ -n "$DNS" ]; then
    DOMAINS="-d $DNS $DOMAINS"
    fi
done
fi
echo "in folder" `pwd`
$SIMP_LE --email $EMAIL -f account_key.json -f fullchain.pem -f key.pem $DOMAINS --default_root $DEFAULT_ROOT --tos_sha256 6373439b9f29d67a5cd4d18cbc7f264809342dbf21cb2ba2fc7588df987a6221 && $SERVICE nginx reload
done
```

Mit allem, was bereits vorhanden ist, sollte es nur fünf Minuten dauern, unseren
Blog dort zu implementieren. In der Realität wurden daraus 2-3 Stunden, dafür hab ich nebenbei auch noch ein bisschen geplaudert und gechattet...
Schaut euch [unser CI Skript](https://gitlab.com/foodsharing-dev/foodsharing-dev.gitlab.io/blob/master/.gitlab-ci.yml)
für Details an. Ihr müsst nur einen privaten SSH Schlüssel in euren Projekt geheimen 
Variablen erstellen, damit Gitlab CI den kompilierten Blog direkt per rsync auf euren Server laden kann.
Achtet darauf, einen RSA Schlüssel ohne einen Kommentar (`-t rsa -C ""`) zu benutzen, da die CI sonst
an dieser Stelle nach einem Passwort fragt und das Deployment abbricht.
Außerdem habe ich es nicht geschafft, die Host-Key-Verifikation zum Laufen zu bringen.
Das ist aber in diesem Szenario nicht wirklich ein Problem.
