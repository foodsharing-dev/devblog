---
layout: post
title:  Zukunftsplanung unserer Plattform foodsharing.de
ref:    foodsharing-dev-continues
date:   2022-08-13 01:25:00 +02:00
lang:   de
orig-lang: de
author: christian-w
---

Gut organisiert Lebensmittel retten und gemeinsam die Zukunft organisieren – dafür brauchen wir jetzt euch!

Damit unsere gemeinsame Plattform, welche die Basis für eure Lebensmittelrettungen ist, weiterleben kann, brauchen wir euch und eure Erfahrungen.

Doch bevor wir dazu kommen, was wir von euch erwarten, möchten wir euch unsere Herausforderungen kurz darstellen.

<!--more-->

# Unsere Herausforderungen:
Jeder Entwickler hat seine Präferenz, was er zum Beispiel gut kann oder was er gern entwickelt. (Diejenigen, die aktiv coden, sind gerade ausschließlich Männer. All genders welcome!) Der eine baut gern Schnittstellen, der andere entwickelt gern die Kern-Funktionen (das sogenannte Backend) und wieder andere gestalten gern die Benutzeroberflächen (das Frontend). Wir möchten euch dabei kurz unsere Hürden darstellen:

Der aktuelle Code der Plattform gibt diese Aufteilung nicht her. Das bedeutet, dass ich in der Arbeit im Code oft viel gleichzeitig im Blick behalten muss. Der Code ist recht alt und eine Dokumentation ist bisher nur an den neueren Stellen vorhanden. Alte und neue Technologien paaren sich und sind bunt gemischt. Die Herausforderung ist es also auch, ein gemeinsames Konzept zu gestalten.

Des Weiteren werden an uns über viele unterschiedliche Kanäle (Produkt-Team, IT-Support (Helpdesk), Slack, Persönliche Nachrichten) Verbesserungsvorschläge oder neue Funktionsvorschläge übermittelt. Manches davon wurde umgesetzt und alle waren glücklich – bis neue Foodsaver, Betriebsverantwortliche und Botschafter kamen, die es genau umgekehrt haben wollten. Uns ist dabei aufgefallen, dass vieles in der Produkt AG nicht zu Ende diskutiert wird und wir Entwickler wissen nicht, was genau die Anforderung ist.

Wir haben uns ein paar Verbesserungsvorschläge überlegt und möchten mit mehr Struktur bei der Entwicklung vorgehen.

# Neue Ausrichtung - unser Zukunftsplan:
Wir müssen erst unsere Altlasten loswerden, bevor wir neue größere Funktionen einbauen. Das ist vergleichbar mit einem Haus: Aktuell sind das Fundament und das Dach marode. Es tropft an allen möglichen Stellen in unserem Foodsharing-Haus. Habt also bitte etwas Geduld, wenn ihr neue Ideen einbringt. Wir entwickeln vorerst keine neuen Funktionen mehr, bevor das Dach repariert ist.
Ideen, die an uns herangetragen werden, sind nicht verloren.

Unser aktuell höchst priorisiertes Ziel ist, für alle Funktionen eine Schnittstelle (REST API) zu bauen. Wenn das passiert ist, kann die Benutzeroberfläche Seite für Seite (also das Frontend) relativ schnell umgebaut werden.

Wir erstellen sogenannte „Slack Fokus Kanäle“ für einzelne Themen, um unsere Arbeit übersichtlich kommunizieren zu können.

# Was wir uns von euch allen wünschen:
Wir wünschen uns mehr Foodsaver, Betriebsverantwortliche und Botschafter, die Slack nutzen und uns Feedback geben.

Unsere Kommunikation findet nämlich hauptsächlich per Chat beim Instant-Messenger-Dienst Slack statt. Im Grunde ist Slack sowas ähnliches wie „Microsoft Teams“ und ermöglicht uns, Themen in Threads zu besprechen, Screenshots/Dateien hochzuladen und per Volltext suchen zu können.
Und so könnt ihr euch Slack einrichten: https://slackin.yunity.org

Wir wünschen uns eine sachliche und wertschätzende Kommunikation.
Das sollte eigentlich selbstverständlich sein. Immer wieder hören wir u.a. Dinge wie „das ist aber doof, das geht doch nicht so”.
Bitte begründet sachlich, warum die Funktion für euch so nicht passt. Sagt uns bitte, warum ihr die Funktion nicht nutzen könnt - was genau wollt ihr machen? Nur so können wir gemeinsam eine Lösung erarbeiten.
Etwa einmal die Woche gibt es eine Telefonkonferenz der Entwickelnden, je nach Bedarf. Das ist auch eine Gelegenheit, direkt über ein Anliegen zu sprechen.

Wir wünschen uns eine engere Zusammenarbeit mit der Produkt-AG.
Das ist insbesondere dafür wichtig, um passende Umfragen erstellen zu können und informierter zu sein, was ihr Foodsaver tatsächlich benötigt. Nur so können wir auch ein Gesamtkonzept erarbeiten, in dem alle Anforderungen erfasst sind.

Wir wünschen uns, dass weitere Entwickelnde ins Team kommen, da
wir, ebenso wie Ihr Lebensmittelretter, die Foodsharing- Plattform ehrenamtlich entwickeln. Ihr seid herzlich willkommen! Mehr Informationen dazu gibt es unter dem Link: https://devdocs.foodsharing.network/it-tasks.html

Vielen Dank!


Es sind stets Personen männlichen und weiblichen Geschlechts gleichermaßen gemeint; aus Gründen der
einfacheren Lesbarkeit wird im Folgenden nur die männliche Form verwendet.
