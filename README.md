<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Jekyll
1. Generate the website: `jekyll build -d public`
1. Preview your project: `jekyll serve`
1. Add content

## Editing

To create a new blog post, just look at one of the existings.
Each post, in the \_post subdirectory, has to have:

* a ref tag to identify the same blog posts in different languages
* a lang tag to define the language
* a date
* an author
* a title
* and a body

### Translating

To translate a blog post, just copy the existing post file to a new name. You may give the file a name in the language you are going to translate to, similar to the title.
Then, start by changing the lang tag. You may add yourself to the list of translators.

## License

This site uses a modified Jekull default theme with multilingual support from https://github.com/sylvaindurand/jekyll-multilingual and is as well as those released under the MIT License.

